
This channel is mostly about chess, matrix and emacs stuff.

To add it as a guix channel, add this to ~/.config/guix/channels.scm:
```scheme
(cons (channel
  (name 'mroh)
  (url "https://gitlab.com/mroh69/guix.git")
  (introduction
   (make-channel-introduction
    "b70f2df4dea8ffabc1a20eadf320c1546671fdb6"
    (openpgp-fingerprint
     "755E 2DE5 D0D5 85C5 2E78  2830 7C7A FFBE FEF2 CB25"))))
 %default-channels)
 ```
