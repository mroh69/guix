; SPDX-FileCopyrightText: 2020 mike@rohleder.de
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (mroh guix packages chess)
  #:use-module (guix packages)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix svn-download)
  #:use-module (guix utils)
  #:use-module (guix gexp)
  #:use-module (guix build-system python)
  #:use-module (guix build-system pyproject)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system go)
  #:use-module (guix build-system qt)
  #:use-module (guix build-system trivial)
  #:use-module (mroh guix packages python)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages base)
  #:use-module (gnu packages check)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages games)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages golang)
  #:use-module (gnu packages tcl)
  #:use-module (gnu packages time)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages pkg-config))

(define-public python-chess
  (package
    (name "python-chess")
    (version "1.9.4")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "chess" version))
       (sha256
        (base32
         "10dkm6j6rsslylkc5lcbqxfsdfyjxwqx3msbwd3bwnlpz1jdchkz"))))
    (build-system python-build-system)
    (home-page "https://github.com/niklasf/python-chess")
    (synopsis "A pure Python chess library")
    (description "A pure Python chess library with move generation and
validation, Polyglot opening book probing, PGN reading and writing,
Gaviota tablebase probing, Syzygy tablebase probing and XBoard/UCI
engine communication.")
    (license license:gpl3)))

(define-public python-berserk
  (package
    (name "python-berserk")
    (version "0.13.2")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/lichess-org/berserk/")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "1yk6adlvyjjhr3wa0d35bw4k9hnkam9wgwmr3vl80jval6h5pqs1"))))
    (build-system pyproject-build-system)
    (propagated-inputs
     (list python-deprecated python-ndjson python-requests
           python-dateutil python-typing-extensions))
    (native-inputs
     (list python-poetry-core python-pytest python-pytest-mock python-pydantic
           python-requests-mock))
    (arguments
     (list
      #:tests? #f                        ; TODO: pydantic needs TypeAdapter.
      #:phases
      #~(modify-phases %standard-phases ;; TODO
          (delete 'sanity-check))))
    (home-page "https://github.com/lichess-org/berserk/")
    (synopsis "Python client for the lichess API.")
    (description "Python client for the lichess API.")
    (license license:gpl3)))

(define-public lichess-bot
  (package
    (name "lichess-bot")
    (version "2023.3.8.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/lichess-bot-devs/lichess-bot/")
             (commit "09e255a7bd713cf594f8966e6cfe9a486cf8b14c")))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "035rpg127l3iqdr6kvbv6r2mq6svv4pmxqhi1viqxwlny05p1hpr"))))
    (build-system python-build-system)
    (inputs
     (list python-chess))
    (arguments
     `(#:tests? #f                      ; no tests
       ))
    (home-page "https://github.com/lichess-bot-devs/lichess-bot/")
    (synopsis "")
    (description "")
    (license license:gpl3)))

(define-public jcchess
  (package
   (name "jcchess")
   (version "0.0.1")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/johncheetham/jcchess")
           (commit "206ed9a18ee1233c2a2e25c4fdf4de1b1b5a9170")))
     (file-name (git-file-name name version))
     (sha256
      (base32
       "0ggrn9kka3z109sxarrpy0cn5y7pj2l3g78by3k9qxdd7xvfbynn"))))
   (build-system python-build-system)
   (inputs
    (list python-chess
          python-pycairo
          python-pygobject
          gtk+))
   (arguments
    `(#:tests? #f                       ; no tests
      #:phases
      (modify-phases %standard-phases
                     (add-after 'unpack 'fix-prefix
                                (lambda _
                                  (let ((out (assoc-ref %outputs "out")))
                                    ;; sys.prefix points to Python's, not our, --prefix.
                                    (mkdir-p (string-append out "/share"))
                                    (substitute* "setup.py"
                                                 (("sys\\.prefix")
                                                  (format #f "\"~a\"" out))))))
                     (add-after 'install 'wrap-screenkey
                                (lambda _
                                  (wrap-program
                                   (string-append (assoc-ref %outputs "out") "/bin/jcchess")
                                   `("GUIX_PYTHONPATH" ":" prefix (,(getenv "GUIX_PYTHONPATH")))
                                   `("GI_TYPELIB_PATH"
                                     ":" prefix (,(getenv "GI_TYPELIB_PATH")))))))))
   (home-page "https://github.com/johncheetham/jcchess")
   (synopsis "A chess GUI to play against chess engines ")
   (description "A chess GUI to play against chess engines ")
   (license license:gpl3)))

(define-public cfish
  (package
    (name "cfish")
    (version "12")
    (home-page "https://github.com/syzygy1/Cfish")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url home-page)
             (commit (string-append name "_" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0gc4sys8wjrqrn0drzig3f6yc9wrdj02aqxqvsnaim4kkqa430nx"))))
    (build-system gnu-build-system)
    (arguments
     `(#:tests? #f
       #:make-flags (list "-C" "src" "build" ; strange, profile-build seems slower
                          (string-append "PREFIX="
                                         (assoc-ref %outputs "out"))
                          "ARCH=x86-64-modern" "numa=no" "native=no" "extra=yes" "nnue=no")
       #:phases (modify-phases %standard-phases
                  (delete 'configure))))
    (supported-systems '("x86_64-linux"))
    (synopsis "C port of stockfish")
    (description "C port of stockfish")
    (license license:gpl3+)))

(define-public stockfish-modern
  (package
    (inherit stockfish)
    (name "stockfish-modern")
    (arguments
     `(,@(substitute-keyword-arguments (package-arguments stockfish)
           ((#:make-flags mf)
            `(list "-C" "src"
                   "build"
                   (string-append "PREFIX="
                                  (assoc-ref %outputs "out"))
                   "ARCH=x86-64-modern")))))
    (supported-systems '("x86_64-linux"))))

(define-public shashchess
  (let ((neural-network-revision "52471d67216a"))
    (package
     (name "shashchess")
     (version "29.1")
     (home-page "https://github.com/amchess/ShashChess")
     (source
      (origin
       (method git-fetch)
       (uri (git-reference
             (url home-page)
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "15lir93np3nmcpprfq85c7j2g952a0p0q48rg1q7absyghvdbspy"))))
     (build-system gnu-build-system)
     (inputs
      `(("neural-network"
         ,(origin
           (method url-fetch)
           (uri (string-append "https://tests.stockfishchess.org/api/nn/nn-"
                               neural-network-revision ".nnue"))
           (sha256
            (base32
             "08128yksbzgr55wh0ds0pann5dxjms5dlpksa2w68xba45kisisj"))))
        ("curl" ,curl)))
     (arguments
      `(#:tests? #f
        #:make-flags (list "-C" "src/All"
                           "build"
                           (string-append "PREFIX=" (assoc-ref %outputs "out"))
                           "ARCH=x86-64-modern")
        #:phases
        (modify-phases %standard-phases
                       (delete 'configure)
                       (add-after 'unpack 'remove-lws2_32 ;whats that?
                                  (lambda _
                                    (substitute* "src/All/Makefile"
                                                 (("-lws2_32") ""))))
                       (add-after 'unpack 'copy-net
                                  (lambda* (#:key inputs #:allow-other-keys)
                                    (copy-file (assoc-ref inputs "neural-network")
                                               (string-append "src/All/nn-"
                                                              ,neural-network-revision
                                                              ".nnue"))))
                       (add-after 'install 'install-doc
                                  (lambda _
                                    (let ((out (assoc-ref %outputs "out")))
                                      (install-file "README.md" (string-append out "/share/doc/" ,name "-" ,version))))))))
     (supported-systems '("x86_64-linux"))
     (synopsis "UCI Chess Engine written in C.")
     (description "UCI Chess Engine written in C.")
     (license license:gpl3+))))

(define-public cutechess
  (package
    (name "cutechess")
    (version "1.3.1")
    (home-page "https://github.com/cutechess/cutechess")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url home-page)
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0z3wm566kv94incapiqy20zz7gbv8b0w1krwl7z6qc1npk0i73iz"))))
    (build-system qt-build-system)
    (native-inputs (list qttools-5))
    (inputs (list qtbase-5 qtsvg-5))
    (arguments
     `(#:tests? #f))
    (synopsis "Cute Chess is a graphical user interface, command-line interface and a library for playing chess.")
    (description "Cute Chess is a graphical user interface, command-line interface and a library for playing chess.")
    (license (list license:gpl3+ license:expat))))

(define-public ordo
  (package
    (name "ordo")
    (version "1.2.6")
    (home-page "https://github.com/michiguel/Ordo")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url home-page)
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0a7wzksv5ys3y27chlnbnfvvjl7yzwb23qdxf6zlgvy1yi4c1c8l"))))
    (build-system gnu-build-system)
    (arguments
     `(#:tests? #f
       #:make-flags '("CFLAGS=-O3 -DNDEBUG -DMY_SEMAPHORES -I myopt -I sysport") ;no native plz
       #:phases
       (modify-phases %standard-phases
         (delete 'configure)
         (replace 'install
           (lambda _
             (let ((out (assoc-ref %outputs "out")))
               (install-file "ordo" (string-append out "/bin"))
               (install-file "README.md" (string-append out "/share/doc/" ,name "-" ,version))))))))
    (synopsis "Ratings for chess and other games")
    (description "Ratings for chess and other games")
    (license license:gpl3+)))

(define-public ordoprep
  (package
    (name "ordoprep")
    (version "0.9.9.8")
    (home-page "https://github.com/michiguel/Ordoprep")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url home-page)
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0jqjg3sxb7rrm566lmw8zprpfmml6ak5sx983xcqvx795bxjfld0"))))
    (build-system gnu-build-system)
    (arguments
     `(#:tests? #f
       #:make-flags '("CFLAGS=-O3 -DNDEBUG -I myopt") ;no native
       #:phases
       (modify-phases %standard-phases
         (delete 'configure)
         (replace 'install
           (lambda _
             (let ((out (assoc-ref %outputs "out")))
               (install-file "ordoprep" (string-append out "/bin"))
               (install-file "README.md" (string-append out "/share/doc/" ,name "-" ,version))))))))
    (synopsis "Preparation of PGN files for rating calculation programs such as Ordo")
    (description "Preparation of PGN files for rating calculation programs such as Ordo")
    (license license:gpl3+)))

;; (define-public wsc
;;   (let ((branch "uciEngineBridge")
;;         (commit "528008eb9157052fc895bae4f98da23ebc348e1e"))
;;     (package
;;       (name "wsc")
;;       (version (git-version "1" branch commit))
;;       (source
;;        (origin
;;          (method git-fetch)
;;          (uri (git-reference
;;                (url "https://gitlab.com/mroh69/wsc.git")
;;                (commit commit)))
;;          (file-name (git-file-name name version))
;;          (sha256
;;           (base32 "0l5bxyp953hry1hbmphjisr28q3a2ysz865y3vmw714850ibb54i"))))
;;       (build-system go-build-system)
;;       (native-inputs (list go-golang-org-x-net))
;;       (arguments
;;        `(#:install-source? #f
;;          #:tests? #f
;;          #:phases
;;          (modify-phases %standard-phases
;;            (replace 'build
;;              (lambda* (#:key import-path #:allow-other-keys)
;;                (with-directory-excursion "src/"
;;                  (invoke "go" "build" "-v" "-x" "-o" "wsc" "main.go"))))
;;            (replace 'install
;;              (lambda* (#:key outputs #:allow-other-keys)
;;                (let ((out (assoc-ref outputs "out")))
;;                  (install-file "src/wsc" (string-append out "/bin"))))))))
;;       (synopsis "uci websocket wrapper")
;;       (description
;;        "connects to a uci chess engine and proxy in/out to a websocket")
;;       (home-page "https://gitlab.com/mroh69/wsc")
;;       (license license:gpl3+))))

(define-public pgn-extract
  (package
    (name "pgn-extract")
    (version "22.11")
    (home-page "https://www.cs.kent.ac.uk/people/staff/djb/pgn-extract/")
    (source (origin
              (method url-fetch)
              (uri "https://www.cs.kent.ac.uk/~djb/pgn-extract/pgn-extract-22-11.tgz")
              (sha256
               (base32 "02qwcb4c1rpljqpg6wn9xm3lrfar7gm954hmgz17v64rabaq87ik"))))
    (build-system gnu-build-system)
    (arguments
     `(#:tests? #f                      ; TODO
       #:phases
       (modify-phases %standard-phases
         (replace 'install
           (lambda _
             (let ((out (assoc-ref %outputs "out")))
               (install-file "pgn-extract" (string-append out "/bin"))
               (install-file "pgn-extract.man" (string-append out "/share/man/man1"))
               (install-file "eco.pgn" (string-append out "/share/" ,name "-" ,version "/")))))
         (delete 'configure))))
    (synopsis "A Portable Game Notation (PGN) Manipulator for Chess Games")
    (description "This is a command-line program for searching,
manipulating and formatting chess games recorded in the Portable Game
Notation (PGN) or something close. It is capable of handling files
containing millions of games. It also recognises Chess960 encodings.")
    (license license:gpl3+)))

(define-public dgtnix
  (package
    (name "dgtnix")
    (version "1.9.1-1")
    (source
     (origin
       (method svn-fetch)
       (uri (svn-reference
             (url "https://svn.code.sf.net/p/dgtnix/code/trunk")
             (revision 237)))
       (sha256
        (base32 "0ni10ghfqxwq2pkdshdixnah5dahh1k24ib0snq14k0v3krc00kd"))))
    (build-system gnu-build-system)
    (arguments
     (list
      #:phases
      #~(modify-phases %standard-phases
          (add-after 'unpack 'set-AM_PROG_AR
            (lambda _
              (substitute* "configure.ac"
                (("^AC_PROG_LIBTOOL.*" orig)
                 (string-append "AM_PROG_AR\n" orig)))))
          (add-after 'set-AM_PROG_AR 'autogen
            (lambda _
              (invoke "autoreconf" "-vif")))
          (add-after 'unpack 'python-2to3
            (lambda _
              (invoke "2to3" "-Wn" "utils/dgtnixTest.py")))
          (add-after 'install 'install-py
            (lambda* (#:key outputs #:allow-other-keys)
              (invoke "2to3" "-Wno" (string-append #$output "/bin") "utils/dgtnix.py"))))))
    (native-inputs (list autoconf automake libtool python-wrapper))
    (home-page "https://dgtnix.sourceforge.net/")
    (synopsis "POSIX driver for the dgt chess devices.")
    (description "POSIX driver for the dgt chess devices.")
    (license license:gpl3+)))

;; WIP
(define-public dgtdrv
  (package
    (name "dgtdrv")
    (version "0.0.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://git.code.sf.net/p/dgtdrv/code.git")
             (commit "master")))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1fp3cxkw731dcx1qdk9dki7vqrm6ksiwy0l7ppc1h26ixnbpqh8f"))))
    (build-system gnu-build-system)
    (home-page "https://dgtdrv.sourceforge.net/")
    (synopsis "dgtdrv aims to be a uci/xboard/crafty compatible interface (input engine) for the DGT Electronic Chess board on POSIX platforms. It relies on the dgtnix library.")
    (description "")
    (license license:gpl3+)))

;; WIP
(define-public scid
  (package
    (name "scid")
    (version "4.7.0")
    (home-page "https://scid.sourceforge.net/")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "mirror://sourceforge/scid/Scid/Scid 4.7/scid-code-" version ".zip"))
       (sha256
        (base32 "1nl61yb03nsc61j98zslwbm4z4yqkz0mjlp8rq7xd0iwxj0mglif"))))
    (build-system gnu-build-system)
    (arguments
     `(#:tests? #f
       #:phases
       (modify-phases %standard-phases
         (add-before 'configure 'remove-native-flag
           (lambda _
             (substitute* "configure"
               (("-march=native ") ""))))
         (replace 'configure
           (lambda* (#:key outputs #:allow-other-keys)
             (let ((out (assoc-ref outputs "out")))
               (invoke "./configure"
                       (string-append "BINDIR=" out "/bin")
                       (string-append "SHAREDIR=" out "/share/" ,name))
               #t)))
         (add-after 'install 'wrap-program
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (tk (assoc-ref inputs "tk"))
                    (tkv ,(let ((v (package-version tk)))
                            (string-take v (string-index-right v #\.))))
                    (dir (string-append tk "/lib/tk" tkv)))
               (wrap-program (string-append out "/bin/scid")
                 `("TK_LIBRARY" "" = (,dir)))
               #t))))))
    (native-inputs
     `(("unzip" ,unzip)
       ("tcl" ,tcl)))
    (inputs
     `(("tk" ,tk)))
    (synopsis "SCID is an open application to view, edit, and manage
collections of chess games")
    (description "Manage databases with millions of games, analyze
using UCI or Winboard engines, prepare for your next opponent, and
much more.")
    (license license:gpl3+)))

(define-public scidvspc
  (package
    (name "scidvspc")
    (version "4.23")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "mirror://sourceforge/scidvspc/source/scid_vs_pc-" version ".tgz"))
       (sha256
        (base32 "128f0wpzy57mq4yqkfw6qhcnwp7cdvaz94szkxbirqnw18vs90p1"))
       ;; Remove unused bundled libraries.
       (modules '((guix build utils)))
       (snippet
        '(begin
           (with-directory-excursion "src"
                                     (for-each delete-file-recursively '("zlib")))
           (delete-file-recursively "timeseal") ;binary for ics
           ))))
    (build-system gnu-build-system)
    (arguments
     `(#:tests? #f
       #:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'remove-root-test
           (lambda _
             (substitute* "Makefile.conf"
               (("~/.fonts") "$(FONTDIR)"))))
         (replace 'configure
           (lambda _
             (let ((out (assoc-ref %outputs "out")))
               (setenv "HOME" "/tmp")
               (invoke "./configure"
                       (string-append "BINDIR=" out "/bin")
                       (string-append "SHAREDIR=" out "/share/" ,name)
                       (string-append "FONTDIR=" out "/fonts/" ,name)) ;XXX
               ))))))
    (native-inputs (list tcl))
    (inputs (list tk zlib))
    (home-page "https://scidvspc.sourceforge.net/")
    (synopsis "")
    (description "")
    (license license:gpl2)))

(define-public laser-chess
  (package
    (name "laser-chess")
    (version "1.7")
    (home-page "https://github.com/jeffreyan11/laser-chess-engine")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url home-page)
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1l3kpyal9jxwmdqdx6l61wmsvvppcq4kkjgbh8apdj2knp2zcr1s"))))
    (build-system gnu-build-system)
    (arguments
     `(#:tests? #f
       #:make-flags (list "-C" "src")
       #:phases
       (modify-phases %standard-phases
         (delete 'configure)
         (replace 'install
           (lambda _
             (let ((out (assoc-ref %outputs "out")))
               (install-file "src/laser" (string-append out "/bin"))))))))
    (synopsis "Laser, a UCI-compliant chess engine")
    (description "Laser is a UCI-compliant chess engine written in
C++11 by Jeffrey An and Michael An.")
    (license license:gpl3+)))

(define-public fire-chess
  (package
    (name "fire-chess")
    (version "10072022")
    (home-page "https://github.com/FireFather/fire")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url home-page)
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "13chypdmaq005xfp7sy4n1l4xfhcxrl6qpzscrg34g61p7fdqjnv"))))
    (build-system gnu-build-system)
    (arguments
     `(#:tests? #f
       #:make-flags (list "build" "ARCH=x86-64-sse41")
       #:phases
       (modify-phases %standard-phases
         (delete 'configure)
         (replace 'install
           (lambda _
             (let ((out (assoc-ref %outputs "out")))
               (install-file "fire" (string-append out "/bin"))))))))
    (synopsis "a strong open-source freeware UCI chess engine")
    (description "a strong open-source freeware UCI chess engine")
    (license license:gpl3+)))

(define-public brainlearn
  (package
   (name "brainlearn")
   (version "21.1")
   (home-page "https://github.com/amchess/BrainLearn")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url home-page)
           (commit version)))
     (file-name (git-file-name name version))
     (sha256
      (base32 "1pkvkazgyav7ms06g17wjs087ax88c7fcg12f2zavwx846dd8qdn"))))
   (build-system gnu-build-system)
   (inputs (list curl))
   (arguments
    `(#:tests? #f
      #:make-flags (list "-C" "src"
                         "build"
                         (string-append "PREFIX=" (assoc-ref %outputs "out"))
                         "ARCH=x86-64-modern")
      #:phases
      (modify-phases %standard-phases
                     (delete 'configure)
                     (replace 'install
                              (lambda _
                                (let* ((out (assoc-ref %outputs "out"))
                                       (doc (string-append out "/share/doc/" ,name "-" ,version)))
                                  (rename-file "src/stockfish" "brainfish")
                                  (install-file "brainfish" (string-append out "/bin"))
                                  (install-file "Readme.md" doc)
                                  (install-file "doc/MASTERING THE GAME OF CHESS THROUGH A RE.pdf" doc)))))))
   (synopsis "Brainlearn, a UCI-compliant chess engine")
   (description "")
   (license license:gpl3+)))

(define-public ethereal
  (package
    (name "ethereal")
    (version "13.00")
    (home-page "https://github.com/AndyGrant/Ethereal")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url home-page)
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0kqk0rphc8a3ypgxc6higynng8agg9d52c1pbnahcr686j1bfr19"))))
    (build-system gnu-build-system)
    (arguments
     `(#:tests? #f
       #:make-flags (list "-C" "src" "popcnt")
       #:phases
       (modify-phases %standard-phases
         (delete 'configure)
         (replace 'install
           (lambda _
             (let ((out (assoc-ref %outputs "out")))
               (install-file "src/Ethereal" (string-append out "/bin"))))))))
    (supported-systems '("x86_64-linux"))
    (synopsis "Ethereal, a UCI Chess Engine.")
    (description
     "Ethereal is a UCI-compliant chess engine which uses the alpha-beta framework.")
    (license license:gpl3+)))

(define-public berserk
  (package
    (name "berserk")
    (version "10")
    (home-page "https://github.com/jhonnold/berserk")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url home-page)
             (commit version)
             (recursive? #t)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "15ymxnda5p53ajfpjp9gar8mh4qc62i4705xc5y4d5z2sgg2jfrx"))))
    (build-system gnu-build-system)
    (inputs
     `(("neural-network"
        ,(origin
           (method url-fetch)
           (uri "https://github.com/jhonnold/berserk-networks/raw/9a6c34609eaec0217c80396792f0b732cca16f29/berserk-e9b73cc3d96f.nn")
           (sha256
            (base32
             "0p4ss7rdw680qm91rdf5pwl2a27z1jw9sb8yrmhanl3gv71krdz9"))))))
    (arguments
     `(#:tests? #f
       #:make-flags (list "-C" "src" "basic" ;; "CFLAGS=-O3 -std=gnu17 -Wall
                          ;; -Wextra -Wshadow"
                          )           ; TODO: no static/native please.
       #:phases
       (modify-phases %standard-phases
         (delete 'configure)
         (add-after 'unpack 'copy-net
           (lambda* (#:key inputs #:allow-other-keys)
             (copy-file (assoc-ref inputs "neural-network")
                        "src/networks/berserk-c982d9682d4e.nn")))
         (add-after 'unpack 'remove-net-target
           (lambda _
             (substitute* "src/makefile"
               ;; Dont depend on net target.
               ((": clone-networks") ": "))))
         (replace 'install
           (lambda _
             (let ((out (assoc-ref %outputs "out")))
               (install-file "src/berserk" (string-append out "/bin"))
               (install-file "README.md" (string-append out "/share/" ,name "-" ,version "/"))))))))
    (supported-systems '("x86_64-linux"))
    (synopsis "UCI Chess Engine written in C.")
    (description "UCI Chess Engine written in C.")
    (license license:gpl3+)))

(define-public xiphos
  (package
   (name "xiphos")
   (version "0.6")
   (home-page "https://github.com/milostatarevic/xiphos")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url home-page)
           (commit (string-append "v" version))))
     (file-name (git-file-name name version))
     (sha256
      (base32 "1999rh5vbk0wq6d21cq7nxxhzbfhy6cpj29nf9v3s6hnd6hg93kk"))))
   (build-system gnu-build-system)
   (arguments
    `(#:tests? #f
      #:make-flags (list "sse" "CFLAGS=-O3 -Wall -fcommon")
      #:phases
      (modify-phases %standard-phases
        (delete 'configure)
        (replace 'install
          (lambda _
            (let ((out (assoc-ref %outputs "out")))
              (install-file "xiphos-sse" (string-append out "/bin"))))))))
   (supported-systems '("x86_64-linux"))
   (synopsis "A UCI Chess Engine.")
   (description "Xiphos is a UCI chess engine.  The engine employs the
modern search techniques and supports multithreading.")
   (license license:gpl3+)))

(define-public rubichess
  (package
    (name "rubichess")
    (version "20221203")
    (home-page "https://github.com/Matthies/RubiChess")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url home-page)
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0x5r5k3iqqwgnd960p8yczp6p4rnvh1lgrlrsgfb7fmrda1q0h9v"))))
    (build-system gnu-build-system)
    (inputs
     `(("neural-network"
        ,(origin
           (method url-fetch)
           (uri "https://github.com/Matthies/NN/raw/main/nn-5e6b321f90-20221001.nnue")
           (sha256
            (base32
             "0fc3c1cyvpm8m11yh030q72xklrvrcd2z4yrw6gsvs9nj0gk4ssy"))))))
    (arguments
     `(#:tests? #f
       #:make-flags (list "-C" "src"
                          "build"
                          "ARCH=x86-64-modern"
                          "EVALFILE=default")
       #:phases
       (modify-phases %standard-phases
         (delete 'configure)
         (add-after 'unpack 'copy-net
           (lambda* (#:key inputs #:allow-other-keys)
             (copy-file (assoc-ref inputs "neural-network")
                        "src/nn-5e6b321f90-20221001.nnue")))
         (add-after 'unpack 'patch-Makefile
           (lambda _
             (substitute* "src/Makefile"
               (("GITVER = ") (string-append "GITVER = " ,version))
               (("GITID = ") (string-append "GITID = " ,version)))))
         (replace 'install
           (lambda _
             (let ((out (assoc-ref %outputs "out")))
               (install-file "src/RubiChess" (string-append out "/bin"))))))))
    (synopsis "Another chess engine.")
    (description "Just another UCI compliant chess engine. Have a look
at the ChangeLog for a detailed feature list.")
    (license license:gpl3+)))

(define-public komodo
  (package
    (name "komodo")
    (version "13")
    (source
     (origin
       (method url-fetch)
       (uri "https://komodochess.com/pub/komodo-13.zip")
       (sha256
        (base32
         "13f9rq8wz3kbcakjbkc85y41xyi264xr3aawvwlbqa1wmj8zr9rl"))))
    (build-system trivial-build-system)
    (arguments
     `(#:modules ((guix build utils))
       #:builder
       (begin
         (use-modules (guix build utils))
         (let* ((output (assoc-ref %outputs "out"))
                (source (assoc-ref %build-inputs "source"))
                (unzip (string-append (assoc-ref %build-inputs "unzip") "/bin/unzip"))
                (patchelf (string-append (assoc-ref %build-inputs "patchelf")
                                         "/bin/patchelf")))
           (zero? (system* unzip source))
           (chdir "komodo-13_201fd6/Linux")
           (invoke patchelf "--set-interpreter"
                   (string-append (assoc-ref %build-inputs "glibc") "/lib/ld-linux-x86-64.so.2")
                   "komodo-13.02-linux")
           (invoke patchelf "--set-rpath"
                   (string-append (assoc-ref %build-inputs "gcc") "/lib")
                   "komodo-13.02-linux")
           (install-file "komodo-13.02-linux"
                         (string-append output "/bin"))
           (chdir "..")
           (install-file "READMEk13.02.html"
                         (string-append output "/share/doc/komodo-13.02"))))))
    (supported-systems '("x86_64-linux"))
    (native-inputs
     `(("patchelf" ,(@ (gnu packages elf) patchelf))
       ("unzip" ,unzip)))
    (inputs
     `(("glibc" ,glibc)
       ("gcc" ,gcc "lib")))
    (home-page "https://komodochess.com")
    (synopsis "Strong Chess engine")
    (description "")
    (license license:public-domain)))
