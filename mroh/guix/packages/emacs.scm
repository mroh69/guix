; SPDX-FileCopyrightText: 2020,2021 mike@rohleder.de
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (mroh guix packages emacs)
  #:use-module (guix packages)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system emacs)
  #:use-module (gnu packages shells)
  #:use-module (gnu packages python)
  #:use-module (gnu packages pdf)
  #:use-module (gnu packages emacs-xyz))

(define-public emacs-ercn
  (package
    (name "emacs-ercn")
    (version "1.1.1")
    (home-page "https://github.com/leathekd/ercn")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url home-page)
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "19jninbf0dhdw3kn4d38bxmklg0v7sh3m9dwj6z69w99r5pcw480"))))
    (build-system emacs-build-system)
    (synopsis "Flexible ERC notifications")
    (description
     "ercn allows for flexible notification rules in ERC. You can
configure it to notify for certain classes of users, query buffers,
certain buffers, etc.")
    (propagated-inputs (list emacs-dash))
    (license license:gpl3)))

(define-public emacs-cypher-mode
  (package
    (name "emacs-cypher-mode")
    (version "0.0.6")
    (home-page "https://github.com/fxbois/cypher-mode")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url home-page)
             (commit "ce8543d7877c736c574a17b49874c9dcdc7a06d6")))
       (sha256
        (base32
         "0vbcq807jpjssabmyjcdkpp6nnx1288is2c6x79dkrviw2xxw3qf"))))
    (build-system emacs-build-system)
    (synopsis "Major mode for editing cypher scripts")
    (description "Major mode for editing cypher scripts.")
    (license license:gpl3)))

(define-public emacs-ob-cypher
  (package
    (name "emacs-ob-cypher")
    (version "0.0.1-1")
    (home-page "https://github.com/zweifisch/ob-cypher")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url home-page)
             (commit "da9f97339474a48d759fc128cee610c0bc9ae6c0")))
       (sha256
        (base32
         "0r4dsbrdxhyyh708pm8fqn423v21xk7a940nbajw9y07n28y5hgk"))))
    (build-system emacs-build-system)
    (propagated-inputs
     (list emacs-dash emacs-cypher-mode emacs-s))
    (synopsis "query neo4j using cypher in org-mode blocks")
    (description "query neo4j using cypher in org-mode blocks")
    (license license:gpl3+)))

(define-public emacs-xwwp
  (package
    (name "emacs-xwwp")
    (version "0.1-1")
    (home-page "https://github.com/canatella/xwwp")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url home-page)
             (commit "f67e070a6e1b233e60274deb717274b000923231")))
       (sha256
        (base32
         "1ikhgi3gc86w7y3cjmw875c8ccsmj22yn1zm3abprdzbjqlyzhhg"))))
    (build-system emacs-build-system)
    (propagated-inputs (list emacs-helm emacs-ivy))
    (synopsis "Enhance the Emacs xwidget-webkit browser")
    (description "This package enhance the integrated xwidget-webkit
browser with hopefully useful functionnalities.")
    (license license:gpl3)))

(define-public emacs-cpio-mode
  (package
    (name "emacs-cpio-mode")
    (version "0.17")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://elpa.gnu.org/packages/cpio-mode-"
             version ".tar"))
       (sha256
        (base32
         "144ajbxmz6amb2234a278c9sl4zg69ndswb8vk0mcq8y9s2abm1x"))))
    (build-system emacs-build-system)
    (home-page "http://elpa.gnu.org/packages/cpio-mode.html")
    (synopsis "Handle cpio archives in the style of dired.")
    (description "")
    (license license:gpl3+)))

(define-public emacs-visual-regexp-steroids
  (package
    (name "emacs-visual-regexp-steroids")
    (version "1.1")
    (home-page "https://github.com/benma/visual-regexp-steroids.el/")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url home-page)
             (commit "a6420b25ec0fbba43bf57875827092e1196d8a9e")))
       (sha256
        (base32
         "1isqa4ck6pm4ykcrkr0g1qj8664jkpcsrq0f8dlb0sksns2dqkwj"))))
    (build-system emacs-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'patch-template-directory
           (lambda* (#:key outputs inputs #:allow-other-keys)
             (make-file-writable "visual-regexp-steroids.el")
             (emacs-substitute-variables "visual-regexp-steroids.el"
               ("vr--command-python-default"
                (string-append
                 (assoc-ref inputs "python")
                 " "
                 (assoc-ref outputs "out")
                 "/share/emacs-visual-regexp-steroids/regexp.py")))))
         (add-after 'install 'install-templates
           (lambda* (#:key outputs #:allow-other-keys)
             (install-file
              "regexp.py"
              (string-append (assoc-ref outputs "out")
                             "/share/emacs-visual-regexp-steroids")))))))
    (propagated-inputs
     (list emacs-visual-regexp emacs-pcre2el))
    (inputs (list python-wrapper))
    (synopsis "Extends visual-regexp to support other regexp engines")
    (description "visual-regexp-steroids is an extension to
visual-regexp which enables the use of modern regexp engines (no more
escaped group parentheses, and other goodies!). In addition to that,
you can optionally use the better regexp syntax to power
isearch-forward-regexp and isearch-backward-regexp.")
    (license license:gpl3+)))

(define-public emacs-jetbrains-darcula-theme
  (package
    (name "emacs-jetbrains-darcula-theme")
    (version "1.0.0")
    (home-page "https://github.com/ianpan870102/jetbrains-darcula-emacs-theme")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url home-page)
             (commit "7a934115238d7b80df230a5ba7a70d866bc18c66")))
       (sha256
        (base32
         "087fj39m7gmi3bx2q983afal3738rc5zxnfs4d4c72z065z7gsss"))))
    (build-system emacs-build-system)
    (synopsis "A complete port of the default JetBrains Darcula theme for Emacs")
    (description "A complete port of the default JetBrains Darcula theme for Emacs.")
    (license license:gpl3)))

(define-public emacs-maelstrom
  (package
    (name "emacs-maelstrom")
    (version "0.5.2")
    (home-page "https://savannah.nongnu.org/projects/maelstrom-el")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://git.savannah.nongnu.org/git/maelstrom-el")
             (commit version)))
       (sha256
        (base32
         "0fsjm0kd77r4295wjnny43crgh2087dnyjk3jb6cm0kf3pa2bl0j"))))
    (build-system emacs-build-system)
    (synopsis "Library and client for the Matrix protocol for Emacs")
    (description "Library and client for the Matrix protocol for Emacs.")
    (license license:gpl3)))

(define-public emacs-shrface
  (package
    (name "emacs-shrface")
    (version "2.6.3")
    (home-page "https://github.com/chenyanming/shrface")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url home-page)
             (commit version)))
       (sha256
        (base32
         "1gg0z15a4rygi0dxabnlpjh56sa7kwxw3fji6ibk70l1jwgd7ydc"))))
    (build-system emacs-build-system)
    (propagated-inputs (list emacs-org))
    (synopsis "Extend shr/eww/nov with org-mode features.")
    (description "Extend shr/eww/nov with org-mode features.")
    (license license:gpl3)))

(define-public emacs-annotate
  (package
    (name "emacs-annotate")
    (version "1.4.3")
    (home-page "https://github.com/bastibe/annotate.el")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url home-page)
             (commit version)))
       (sha256
        (base32
         "169nwa7jfsdcjk6mbm3yabk3j8iwfixfkypwk5336dy2ncf90cjc"))))
    (build-system emacs-build-system)
    (synopsis "Annotate.el")
    (description "This package provides a minor mode annotate-mode,
which can add annotations to arbitrary files without changing the
files themselves.")
    (license license:expat)))

(define-public emacs-sudo-utils
  (package
    (name "emacs-sudo-utils")
    (version "3.0.1")
    (home-page "https://github.com/alpha-catharsis/sudo-utils")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url home-page)
             (commit (string-append "v" version))))
       (sha256
        (base32
         "03kks8sqw1j8ywzk3bvcb8i6v3px5zr05c4pnrwmls724m79sagd"))))
    (build-system emacs-build-system)
    (synopsis "sudo utilities for emacs")
    (description "This package provides some useful interactive and
non-interactive functions for working with sudo shell program.")
    (license license:gpl3)))

