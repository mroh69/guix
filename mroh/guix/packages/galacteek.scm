; SPDX-FileCopyrightText: 2020 mike@rohleder.de
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (mroh guix packages galacteek)
  #:use-module (guix packages)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (guix build-system python)
  #:use-module (gnu packages check)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages time)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages web)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages protobuf)
  #:use-module (gnu packages python-crypto)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-web))

(define-public python-aioipfs
  (package
    (name "python-aioipfs")
    (version "0.5.2")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "aioipfs" version))
       (sha256
        (base32
         "0r4w6pmbjww3snk5b07b697bqqx0nwigjbc7h27gpv34l1ndm7p4"))))
    (build-system python-build-system)
    (propagated-inputs
     (list python-aiofiles python-aiohttp python-base58))
    (home-page "https://gitlab.com/cipres/aioipfs")
    (synopsis "Asynchronous IPFS client library")
    (description "Asynchronous IPFS client library")
    (license license:lgpl3)))

(define-public python-ipfshttpclient
  (package
    (name "python-ipfshttpclient")
    (version "0.7.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "ipfshttpclient" version))
       (sha256
        (base32 "14rnqk61fqa6c1ql412q723g7spgpv2pch96h7p8gb632hy07cgy"))))
    (build-system python-build-system)
    (arguments `(#:tests? #f))
    (propagated-inputs (list python-multiaddr python-requests))
    (home-page
     "https://ipfs.io/ipns/12D3KooWEqnTdgqHnkkwarSrJjeMP2ZJiADWLYADaNvUb6SQNyPF/")
    (synopsis "Python IPFS HTTP CLIENT library")
    (description "Python IPFS HTTP CLIENT library")
    (license license:expat)))

(define-public python-ipfsapi
  (package
    (name "python-ipfsapi")
    (version "0.4.4")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "ipfsapi" version))
       (sha256
        (base32 "17ddd54dxvvqc7a7bx0w8jp9bjcfik120lkks9nk4ph4mcn5zplg"))))
    (build-system python-build-system)
    (arguments
     `(#:tests? #f
       #:phases
       (modify-phases %standard-phases
         (delete 'sanity-check))))
    (propagated-inputs (list python-ipfshttpclient python-netaddr python-six))
    (home-page "https://github.com/ipfs/py-ipfs-http-client/tree/py-ipfs-api")
    (synopsis "Python IPFS API client library")
    (description "Python IPFS API client library")
    (license license:expat)))

;; WIP
(define-public ipvc
  (package
    (name "ipvc")
    (version "0.1.3")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "ipvc" version))
       (sha256
        (base32 "1vfvn4wknf7mdimrqa8pmcjqhrx5gjvzakpn5x43ivr0587rm0a0"))))
    (build-system python-build-system)
    (propagated-inputs (list python-ipfsapi python-protobuf))
    (home-page "https://github.com/rememberberry/ipvc")
    (synopsis "Inter-Planetary Version Control (System)")
    (description "Inter-Planetary Version Control (System)")
    (license license:expat)))

(define-public python-quamash
  (package
    (name "python-quamash")
    (version "0.6.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "Quamash" version))
       (sha256
        (base32
         "0p0vysc1pm2vcla1g8d7wkzcsb68crsvi0m0clg5j83wpv4sccba"))))
    (build-system python-build-system)
    (arguments
     ;; test fail
     `(#:tests? #f))
    (native-inputs
     `(("python-pytest" ,python-pytest)))
    (propagated-inputs
     `(("python-pyqt" ,python-pyqt)))
    (home-page "https://github.com/harvimt/quamash")
    (synopsis
     "Implementation of the PEP 3156 Event-Loop with Qt.")
    (description
     "Implementation of the PEP 3156 Event-Loop with Qt.")
    (license license:bsd-3)))

(define-public python-feedgen
  (package
    (name "python-feedgen")
    (version "0.9.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "feedgen" version))
       (sha256
        (base32
         "0jl0b87l7v6c0f1nx6k81skjhdj5i11kmchdjls00mynpvdip0cf"))))
    (build-system python-build-system)
    (arguments
     ;; test fail
     `(#:tests? #f))
    (propagated-inputs
     `(("python-dateutil" ,python-dateutil)
       ("python-lxml" ,python-lxml)))
    (home-page
     "https://lkiesow.github.io/python-feedgen")
    (synopsis "Feed Generator (ATOM, RSS, Podcasts)")
    (description
     "Feed Generator (ATOM, RSS, Podcasts)")
    (license license:expat)))

(define-public python-json-traverse
  (package
    (name "python-json-traverse")
    (version "0.4")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "json-traverse" version))
       (sha256
        (base32
         "1ybxf20y8xwf4cbpvz7r9j26spibjz4vr2hbvhwfc1fdlc4vain3"))))
    (build-system python-build-system)
    (home-page
     "https://github.com/EmilStenstrom/json-traverse/")
    (synopsis
     "Query complex JSON structures using a simple query syntax.")
    (description
     "Query complex JSON structures using a simple query syntax.")
    (license license:expat)))

(define-public python-multiaddr
  (package
    (name "python-multiaddr")
    (version "0.0.9")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "multiaddr" version))
       (sha256
        (base32
         "1kqfmcbv8plpicbygwpdljin7n82iyxklc0w1ywxbhzdi58nkcih"))))
    (build-system python-build-system)
    (arguments `(#:tests? #f))
    (propagated-inputs
     `(("python-base58" ,python-base58)
       ("python-netaddr" ,python-netaddr)
       ("python-six" ,python-six)
       ("python-pytest-runner" ,python-pytest-runner)
       ("python-varint" ,python-varint)))
    (home-page
     "https://github.com/multiformats/py-multiaddr")
    (synopsis
     "Python implementation of jbenet's multiaddr")
    (description
     "Python implementation of jbenet's multiaddr")
    (license license:expat)))

(define-public python-py-multibase
  (package
    (name "python-py-multibase")
    (version "1.0.3")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "py-multibase" version))
       (sha256
        (base32
         "1r3z4qqq9bm6mvn3m4zxzy0flz4w6azs09sqyllfq7mnrgpj12nj"))))
    (build-system python-build-system)
    (arguments
     `(#:tests? #f
       #:phases
       (modify-phases %standard-phases
         (delete 'sanity-check))))
    (propagated-inputs (list python-six python-pytest-runner))
    (home-page
     "https://github.com/multiformats/py-multibase")
    (synopsis "Multibase implementation for Python")
    (description
     "Multibase implementation for Python")
    (license license:expat)))

(define-public python-py-multicodec
  (package
    (name "python-py-multicodec")
    (version "0.2.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "py-multicodec" version))
       (sha256
        (base32
         "0iy6grvcw6mwzp5fcp3kyklwhrzsksrwasxqnlcjs9qfikz1y0l3"))))
    (build-system python-build-system)
    (arguments
     `(#:tests? #f
       #:phases
       (modify-phases %standard-phases
         (delete 'sanity-check))))
    (propagated-inputs
     `( ;; ("python-morphys" ,python-morphys)
       ("python-six" ,python-six)
       ("python-pytest-runner" ,python-pytest-runner)
       ("python-varint" ,python-varint)))
    (home-page
     "https://github.com/multiformats/py-multicodec")
    (synopsis "Multicodec implementation in Python")
    (description
     "Multicodec implementation in Python")
    (license license:expat)))

(define-public python-pymultihash
  (package
   (name "python-pymultihash")
   (version "0.8.2")
   (source
    (origin
     (method url-fetch)
     (uri (pypi-uri "pymultihash" version))
     (sha256
      (base32
       "0nr1mxwklfp06bbdxx2q08zxm19ngdcx8r4h4lnx5ipcx4d5mis9"))))
   (build-system python-build-system)
   (arguments `(#:tests? #f))
   (home-page
    "https://github.com/ivilata/pymultihash")
   (synopsis
    "Python implementation of the multihash specification")
   (description
    "Python implementation of the multihash specification")
   (license license:expat)))

(define-public python-varint
  (package
    (name "python-varint")
    (version "1.0.2")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "varint" version))
       (sha256
        (base32
         "19ac46adalgva1chlh0rxv6cinpikxfd92kabbbfjpmcfwiw1v56"))))
    (build-system python-build-system)
    (home-page
     "http://github.com/fmoo/python-varint")
    (synopsis "Simple python varint implementation")
    (description
     "Simple python varint implementation")
    (license license:expat)))

(define-public python-galacteek
  (package
    (name "python-galacteek")
    (version "0.4.37")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "galacteek" version))
       (sha256
        (base32
         "1y57j056rprs9yd95iz4r6x2ywk0p23zqrnphdj7717sadlfx4xz"))))
    (build-system python-build-system)
    (propagated-inputs
     `(("python-aiodns" ,python-aiodns)
       ("python-aiofiles" ,python-aiofiles)
       ("python-aiohttp" ,python-aiohttp)
       ("python-aioipfs" ,python-aioipfs)
       ("python-aiosqlite" ,python-aiosqlite)
       ("python-async-generator" ,python-async-generator)
       ("python-async-timeout" ,python-async-timeout)
       ("python-base58" ,python-base58)
       ("python-feedgen" ,python-feedgen)
       ("python-feedparser" ,python-feedparser)
       ("python-gitpython" ,python-gitpython)
       ("python-jinja2" ,python-jinja2)
       ("python-json-traverse" ,python-json-traverse)
       ("python-jsonschema" ,python-jsonschema)
       ("python-logbook" ,python-logbook)
       ("python-magic" ,python-magic)
       ("python-markdown" ,python-markdown)
       ("python-multiaddr" ,python-multiaddr)
       ("python-pillow" ,python-pillow)
       ("python-protobuf" ,python-protobuf)
       ("python-py-multibase" ,python-py-multibase)
       ("python-py-multicodec" ,python-py-multicodec)
       ("python-pycryptodomex" ,python-pycryptodomex)
       ("python-pygments" ,python-pygments)
       ("python-pymultihash" ,python-pymultihash)
       ("python-pyqt" ,python-pyqt)
       ("python-pyqtwebengine" ,python-pyqtwebengine)
       ;; ("python-pyzbar" ,python-pyzbar)
       ("python-qrcode" ,python-qrcode)
       ("python-quamash" ,python-quamash)
       ;; ("python-web3" ,python-web3)
       ))
    (home-page
     "https://github.com/eversum/galacteek")
    (synopsis "Browser for the distributed web")
    (description "Browser for the distributed web")
    (license license:gpl3+)))
