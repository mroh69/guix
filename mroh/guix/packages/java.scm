; SPDX-FileCopyrightText: 2020 mike@rohleder.de
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (mroh guix packages java)
  #:use-module (guix packages)
  #:use-module (guix licenses)
  #:use-module (guix download)
  #:use-module (guix records)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system trivial)
  #:use-module (gnu services)
  #:use-module (gnu services shepherd)
  #:use-module (gnu services admin)
  #:use-module (gnu system accounts)
  #:use-module (gnu system shadow)
  #:use-module (gnu packages base)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages java)
  #:use-module (ice-9 match)
  #:export (elasticsearch-service-type))

; from bug 30803
(define-public elasticsearch
  (package
   (name "elasticsearch")
   (version "2.4.6")
   (source
    (origin
     (method url-fetch)
     (uri (string-append
           "https://download.elastic.co/elasticsearch/elasticsearch/elasticsearch-"
           version ".tar.gz"))
     (sha256
      (base32 "0vzw9kpyyyv3f1m5sy9zara6shc7xkgi5xm5qbzvfywijavlnzjz"))))
   (build-system gnu-build-system)
   (inputs
    `(("jre" ,icedtea)
      ("coreutils" ,coreutils)
      ("inetutils" ,inetutils)
      ("util-linux" ,util-linux)
      ("grep" ,grep)))
   (arguments
    `(#:phases
      (modify-phases %standard-phases
                     (delete 'check)
                     (delete 'configure)
                     (delete 'build)
                     (replace 'install
                              (lambda* (#:key inputs outputs #:allow-other-keys)
                                (let ((out (assoc-ref outputs "out")))
                                  (for-each
                                   (lambda (dir)
                                     (copy-recursively dir (string-append out "/" dir)
                                                       #:log (%make-void-port "w")))
                                   '("bin" "config" "lib" "modules"))
                                  (for-each
                                   (lambda (dir)
                                     (mkdir (string-append out "/" dir)))
                                   '("plugins"))
                                  (for-each
                                   delete-file
                                   (find-files
                                    (string-append out "/lib")
                                    (lambda (name stat)
                                      (or (string-contains name "freebsd")
                                          (string-contains name "solaris")))))
                                  (wrap-program
                                   (string-append out "/bin/elasticsearch")
                                   `("PATH" = (,(string-append (assoc-ref inputs "util-linux")
                                                               "/bin")
                                               ,(string-append (assoc-ref inputs "coreutils")
                                                               "/bin")
                                               ,(string-append (assoc-ref inputs "inetutils")
                                                               "/bin")
                                               ,(string-append (assoc-ref inputs "grep")
                                                               "/bin")))
                                   `("JAVA_HOME" = (,(assoc-ref inputs "jre"))))))))))
   (home-page "")
   (synopsis "")
   (description "")
   (license public-domain)))

(define-record-type* <elasticsearch-configuration>
  elasticsearch-configuration make-elasticsearch-configuration
  elasticsearch-configuration?
  (elasticsearch     elasticsearch-configuration-elasticsearch
                     (default elasticsearch))
  (data-path         elasticsearch-configuration-data-path
                     (default "/var/lib/"))
  (logs-path         elasticsearch-configuration-logs-path
                     (default "/var/log/elasticsearch"))
  (http-port         elasticsearch-configuration-port
                     (default 9200))
  (transport-port    elasticsearch-configuration-transport-port
                     (default 9300)))

(define (elasticsearch-configuration-directory
         data-path logs-path http-port transport-port)
  (computed-file
   "elasticsearch-config"
   #~(begin
       (mkdir #$output)
       (mkdir (string-append #$output "/scripts"))
       (call-with-output-file (string-append #$output "/elasticsearch.yml")
         (lambda (port)
           (display
            (string-append
             "path.data: " #$data-path "\n"
             "path.logs: " #$logs-path "\n"
             "http.port: " #$(number->string http-port) "\n"
             "transport.tcp.port: " #$(number->string transport-port) "\n")
            port))))))

(define %elasticsearch-accounts
  (list (user-group (name "elasticsearch") (system? #t))
        (user-account
         (name "elasticsearch")
         (group "elasticsearch")
         (system? #t)
         (comment "Elasticsearch server user")
         (home-directory "/var/empty")
         (shell (file-append shadow "/sbin/nologin")))))

(define elasticsearch-activation
  (match-lambda
    (($ <elasticsearch-configuration> elasticsearch data-path logs-path
                                      http-port transport-port)
     #~(begin
         (use-modules (guix build utils)
                      (ice-9 match))

         (let ((user (getpwnam "elasticsearch")))
           ;; Create db state directory.
           (for-each
            (lambda (path)
              (mkdir-p path)
              (chown path (passwd:uid user) (passwd:gid user)))
            '(#$data-path #$logs-path "/var/run/elasticsearch")))))))

(define elasticsearch-shepherd-service
  (match-lambda
    (($ <elasticsearch-configuration> elasticsearch data-path logs-path
                                      http-port transport-port)
     (list (shepherd-service
            (provision '(elasticsearch))
            (documentation "Run the Elasticsearch daemon.")
            (requirement '(user-processes syslogd))
            (start #~(make-forkexec-constructor
                      (list
                       (string-append #$elasticsearch "/bin/elasticsearch")
                       "-d"
                       "-p" "/var/run/elasticsearch/pid"
                       (string-append
                        "-Dpath.conf="
                        #$(elasticsearch-configuration-directory
                           data-path logs-path http-port transport-port)))
                      #:user "elasticsearch"
                      #:pid-file "/var/run/elasticsearch/pid"
                      #:log-file "/var/log/elasticsearch.log"))
            (stop #~(make-kill-destructor)))))))

(define elasticsearch-service-type
  (service-type (name 'elasticsearch)
		(description "Start Elasticsearch.")
                (extensions
                 (list (service-extension shepherd-root-service-type
                                          elasticsearch-shepherd-service)
                       (service-extension rottlog-service-type
                                          (const
                                           (list (log-rotation
                                                  (files (list "/var/log/elasticsearch.log"))))))
                       (service-extension activation-service-type
                                          elasticsearch-activation)
                       (service-extension account-service-type
                                          (const %elasticsearch-accounts))))
                (default-value (elasticsearch-configuration))))

(define-public neo4j
  (package
   (name "neo4j")
   (version "3.5.12")
   (source
    (origin
     (method url-fetch)
     (uri "https://debian.neo4j.org/repo/stable/neo4j_3.5.12_all.deb")
     (sha256
      (base32
       "0122bzl1y4bb2jg0p3sd13wf9zar0hgxf1qw3j4kvhjw2h1fq6ih"))))
   (build-system trivial-build-system)
   (native-inputs
    `(("tar" ,tar)
      ("xz" ,(@ (gnu packages compression) xz))
      ("binutils" ,binutils)))
   (arguments
    `(#:modules ((guix build utils))
      #:builder
      (begin
        (use-modules (guix build utils))
        (let* ((output (assoc-ref %outputs "out"))
               (source (assoc-ref %build-inputs "source"))
               (tar (string-append (assoc-ref %build-inputs "tar") "/bin/tar"))
               (ar (string-append (assoc-ref %build-inputs "binutils") "/bin/ar")))
          ;; Extraction phase
          (mkdir-p output)
          (setenv "PATH" (string-append (assoc-ref %build-inputs "xz") "/bin"))
          (zero? (system* ar "x" source "data.tar.xz"))
          (zero? (system* tar "xvf" "data.tar.xz" "-C" output))))))
   (home-page "https://www.neo4j.org")
   (synopsis "")
   (description "")
   (license public-domain)))
