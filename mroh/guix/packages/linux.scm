; SPDX-FileCopyrightText: 2020,2021 mike@rohleder.de
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (mroh guix packages linux)
  #:use-module (srfi srfi-1)
  #:use-module (guix packages)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix utils)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system qt)
  #:use-module (guix gexp)
  #:use-module (gnu packages)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages check)
  #:use-module (gnu packages docbook)
  #:use-module (gnu packages gettext)

  #:use-module (gnu packages rust)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages gstreamer)

  ;; gstreamer
  #:use-module (gnu packages curl)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages video)
  #:use-module (gnu packages gnupg)
  #:use-module (gnu packages xiph)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages telephony)
  #:use-module (gnu packages ssh)
  #:use-module (gnu packages libusb)
  #:use-module (gnu packages image)
  #:use-module (gnu packages rdf)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages graphics)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages python)
  #:use-module (gnu packages mp3)

  #:use-module (gnu packages markup)
  #:use-module (gnu packages cpp)
  #:use-module (gnu packages crypto)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages logging)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages networking)
  
  #:use-module (gnu packages messaging)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages file-systems)
  #:use-module (gnu packages kodi)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages nfs)
  #:use-module (gnu packages onc-rpc)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages time)
  #:use-module (gnu packages patchutils)
  #:use-module (gnu packages monitoring)
  #:use-module (gnu packages pdf)
  #:use-module (gnu packages python)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages xml)
  #:use-module (gnu services)
  #:use-module (gnu services shepherd)
  #:use-module (gnu system shadow)
  #:use-module (nongnu packages nvidia)

  #:export (rwhod-service-type)
  #:export (mroh-tlp)
  #:export (mroh-linux))

(define-public v4l2loopback-linux-mroh-module
  (package
    (inherit v4l2loopback-linux-module)
    (name "v4l2loopback-linux-mroh-module")
    (inputs
     (modify-inputs (package-inputs v4l2loopback-linux-module)
       (replace "linux" mroh-linux)))))

(define-public mroh-linux
  (let ((version (package-version linux-libre-6.1)))
    (package
     (inherit
      (customize-linux
       #:name "mroh-linux"
       #:source
       (origin
        (inherit (package-source linux-libre-6.1))
        (method url-fetch)
        (uri
         (list (string-append "mirror://kernel.org/linux/kernel/v"
                              (version-major version) ".x/linux-" version ".tar.xz")))
        (sha256
         (base32 "1yzwp0496j63c6lhvsni1ynr8b2cpn552pli3nd3fdk0pp4nqwqy"))
        (patches '()))
       #:defconfig (local-file "kernel-6.1.config"))))))

(define-public mroh-nvidia-module
  (package
   (inherit nvidia-module)
   (name "mroh-nvidia-module")
   (arguments
    `(,@(substitute-keyword-arguments
         (package-arguments nvidia-module)
         ((#:linux li)
          mroh-linux))))))

(define-public mroh-nvidia-driver
  (package
   (inherit nvidia-driver)
   (name "mroh-nvidia-driver")
   (inputs
    (modify-inputs (package-inputs nvidia-driver)
                   (replace "linux-lts" mroh-linux)))))

(define-public mroh-perf
  (package
    (inherit perf)
    (name "mroh-perf")
    (version (package-version mroh-linux))
    (source (package-source mroh-linux))))

(define-public mroh-x86-energy-perf-policy
  (package
    (inherit x86-energy-perf-policy)
    (name "mroh-x86-energy-perf-policy")
    (version (package-version mroh-linux))
    (source (package-source mroh-linux))))

(define-public mroh-tlp
  (package
    (inherit tlp)
    (name "mroh-tlp")
    (inputs
     (modify-inputs (package-inputs tlp)
       (replace "x86-energy-perf-policy" mroh-x86-energy-perf-policy)))))

(define netkit-rwho-source
  (origin
    (method url-fetch)
    (uri "mirror://debian/pool/main/n/netkit-rwho/netkit-rwho_0.17.orig.tar.gz")
    (sha256
     (base32 "19wd32hh79nczyqcdiapn1f9gfs9zknhyg8r5k5xmcpx9g7f4284"))))

(define-public netkit-rwho
  (package
    (name "netkit-rwho")
    (version "0.17-15")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://debian/pool/main/n/netkit-rwho/netkit-rwho_" version ".debian.tar.xz"))
      (sha256
       (base32 "0bdhhrc4jdpifycdpcaygkk4914fng3g03h2h442js7bz5dpzm5m"))))
    (build-system cmake-build-system)
    (arguments
     '(#:tests? #f
       #:phases
       (modify-phases %standard-phases
         (add-before 'unpack 'unpack-org
           (lambda* (#:key inputs native-inputs #:allow-other-keys)
             (invoke "tar" "xf" (assoc-ref inputs "netkit-rwho-source"))))
         (add-after 'unpack 'patch-debian
           (lambda _
             (chdir "../netkit-rwho-0.17")
             (setenv "QUILT_PATCHES" "../debian/patches")
             (invoke "quilt" "push" "-a"))))))
    (native-inputs
     `(("netkit-rwho-source" ,netkit-rwho-source)
       ("quilt" ,quilt)))
    (home-page "https://tracker.debian.org/pkg/netkit-rwho")
    (synopsis "")
    (description "")
    (license license:gpl2)))

(define %rwhod-spool-dir "/var/spool/rwho")

(define %rwhod-accounts
  (list (user-group (name "rwhod") (system? #t))
        (user-account
         (name "rwhod")
         (group "rwhod")
         (system? #t)
         (comment "rwhod user account")
         (home-directory %rwhod-spool-dir)
         (shell (file-append shadow "/sbin/nologin")))))

(define (rwhod-activation config)
  #~(begin
      (use-modules (guix build utils))
      (unless (file-exists? #$%rwhod-spool-dir)
        (mkdir-p #$%rwhod-spool-dir))
      (chmod #$%rwhod-spool-dir #o755)
      (let ((user (getpwnam "rwhod")))
        (chown #$%rwhod-spool-dir (passwd:uid user) (passwd:gid user)))))

(define (rwhod-shepherd-service config)
  (list (shepherd-service
         (provision '(rwhod))
         (documentation "Run the rwhod server.")
         (requirement '(networking))
         (start #~(make-system-constructor
                   #$(file-append netkit-rwho "/sbin/rwhod -b")))
         (stop #~(make-kill-destructor))))) ; FIXME

(define rwhod-service-type
  (service-type
   (name 'rwhod)
   (description "Run the rwhod server.")
   (extensions
    (list (service-extension activation-service-type
                             rwhod-activation)
          (service-extension account-service-type
                             (const %rwhod-accounts))
          (service-extension shepherd-root-service-type
                             rwhod-shepherd-service)))
   (default-value '())))

(define-public mroh-autofs
  (package
    (inherit autofs)
    (name "mroh-autofs")
    (arguments
     (substitute-keyword-arguments (package-arguments autofs)
       ((#:configure-flags cf)
        #~(list "--enable-ignore-busy"
                "--enable-sloppy-mount"
                "--with-libtirpc"))))
    (inputs
     (list
      e2fsprogs
      libtirpc
      nfs-utils
      util-linux))))

(define-public nsntrace
  (package
    (name "nsntrace")
    (version "4.0.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/nsntrace/nsntrace")
             (commit "v4")))
       (file-name (git-file-name name version))
       (sha256
        (base32 "184938x0s5qyhx6cnswgkv868i0m8px47z9h7mzd4gmv0w31h46r"))))
    (build-system gnu-build-system)
    (arguments
     `(#:tests? #f                      ; needs network
       #:phases
       (modify-phases %standard-phases
         (add-before 'configure 'fix-docbook
           (lambda* (#:key inputs #:allow-other-keys)
             (substitute* "man/man.xsl"
               (("http://docbook.sourceforge.net/release/xsl/current/manpages/docbook.xsl")
                (string-append (assoc-ref inputs "docbook-xsl")
                               "/xml/xsl/docbook-xsl-"
                               ,(package-version docbook-xsl)
                               "/manpages/docbook.xsl"))))))))
    (native-inputs
     (list
      autoconf
      automake
      docbook-xsl
      pkg-config))
    (inputs
     (list
      iptables
      libnl
      libpcap
      libxslt))
    (synopsis
     "Perform network trace of a single process by using network namespaces")
    (description "This application uses Linux network namespaces to perform
network traces of a single application.  The traces are saved as pcap
files.  And can later be analyzed by for instance Wireshark.")
    (home-page "https://github.com/nsntrace/nsntrace")
    (license license:gpl2)))

(define-public 6tunnel
  (package
    (name "6tunnel")
    (version "0.13")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/wojtekka/6tunnel")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0zsx9d6xz5w8zvrqsm8r625gpbqqhjzvjdzc3z8yix668yg8ff8h"))))
    (build-system gnu-build-system)
    (arguments
     `(#:phases (modify-phases %standard-phases
                  (add-before 'bootstrap 'autoreconf
                    (lambda _
                      (invoke "autoreconf" "-vfi"))))))
    (native-inputs
     (list
      autoconf
      automake
      python2-minimal))
    (synopsis
     "Tunnelling for application that don't speak IPv6 ")
    (description "Tunnelling for application that don't speak IPv6 ")
    (home-page "https://github.com/wojtekka/6tunnel")
    (license license:gpl2)))

;; WIP
(define-public sslscan
  (package
    (name "sslscan")
    (version "2.0.15")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/rbsec/sslscan")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1b6q1qj7b9xcjqxyla8bc9hy0hkds7qjfnc53rc6653bhxjk6iv1"))))
    (build-system gnu-build-system)
    (arguments
     `(#:tests? #f                      ; needs docker, sigh
       #:phases
       (modify-phases %standard-phases
         (delete 'configure))))
    (inputs (list openssl))
    (synopsis
     "sslscan tests SSL/TLS enabled services to discover supported cipher suites")
    (description "")
    (home-page "https://github.com/rbsec/sslscan")
    (license license:gpl3+)))

;; on arch its moved to https://github.com/zapping-vbi/zvbi
(define-public libzvbi
  (package
    (name "libzvbi")
    (version "0.2.35")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://sourceforge/zapping/zvbi/" version "/zvbi-" version ".tar.bz2"))
       (sha256
        (base32 "062kfvj6f7mkg86aijwdwpcdh42npfrb349zg157qj0s24s3r27w"))))
    (build-system gnu-build-system)
    (arguments
     (list
      #:tests? #f                       ;TODO
      #:configure-flags #~(list "--enable-static=no")))
    (synopsis
     "")
    (description "The ZVBI library provides functions to read from Linux V4L, V4L2 and
  FreeBSD BKTR raw VBI capture devices, from Linux DVB devices and
  from a VBI proxy to share V4L and V4L2 VBI devices between multiple
  applications.

  It can demodulate raw to sliced VBI data in software, with support
  for a wide range of formats, has functions to decode several popular
  services including Teletext and Closed Caption, a Teletext cache
  with search function, various text export and rendering functions.
")
    (home-page "http://sourceforge.net/projects/zapping")
    (license license:gpl3+)))

(define-public mroh-ffmpeg
  (package
    (inherit ffmpeg)
    (name "mroh-ffmpeg")
    (arguments
     (substitute-keyword-arguments (package-arguments ffmpeg)
       ((#:configure-flags flags ''())
        #~(cons "--enable-libzvbi" #$flags))))
    (inputs
     (modify-inputs (package-inputs ffmpeg)
       (append libzvbi)))))

(define-public mroh-mpv
  (package
    (inherit mpv)
    (name "mroh-mpv")
    (inputs
     (modify-inputs (package-inputs mpv)
       (replace "ffmpeg" mroh-ffmpeg)))))

;; stolen from https://gitlab.com/guix-gaming-channels/games/-/blob/master/games/packages/diablo.scm thx efraim!
;; TODO: Unbundle libsmacker, stormlib, radon, pkware, CharisSILB.ttf
(define-public devilutionx
  (package
    (name "devilutionx")
    (version "1.2.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference (url "https://github.com/diasurgical/devilutionX")
                           (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0xclnbbcsp5f5d5y0za95kwzxqi4qf3fzy12p9scgvv9xls2a1iy"))
       (modules
        '((guix build utils)))
       (snippet
        '(begin
           (delete-file-recursively "3rdParty/asio")))))
    (build-system cmake-build-system)
    (arguments
     `(#:tests? #f                      ; TODO: enable tests
       #:configure-flags
       (list (string-append "-DVERSION_NUM=" ,version)
             ;; "-DSPAWN=ON"
             )
       #:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'patch-source
           (lambda* (#:key inputs #:allow-other-keys)
             (let ((asio (assoc-ref inputs "asio")))
               (substitute* "CMakeLists.txt"
                 (("3rdParty/asio") asio)
                 (("sodium_USE_STATIC_LIBS ON") "sodium_USE_STATIC_LIBS OFF"))))))))
    (native-inputs
     `(("asio" ,asio)
       ("pkg-config" ,pkg-config)))
    (inputs
     `(("libsodium" ,libsodium)
       ("sdl2-mixer" ,sdl2-mixer)
       ("sdl2-ttf" ,sdl2-ttf)))
    (home-page "https://github.com/diasurgical/devilutionX")
    (synopsis "Diablo build for modern operating systems")
    (description "DevilutionX is a port of the classic action role-playing hack and slash PC game \"Diablo\" for modern operating systems.  The engine now has support for upscaling, unlocked FPS, controller support, and multiplayer via TCP.")
    (license license:public-domain))) ; CharisSILB.ttf

;; WIP
(define-public flatseal
  (package
    (name "flatseal")
    (version "1.7.5")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/tchx84/flatseal")
                    (commit (string-append "v" version))))
              (file-name (git-file-name "flatseal" version))
              (sha256
               (base32
                "1cand2f9n1l5wcggwbvnbv8ynq058izyqrzpjhwkgyi9628c9icm"))))
    (build-system meson-build-system)
    (arguments
     '(#:tests? #f
       #:glib-or-gtk? #t
       #:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'skip-gtk-update-icon-cache
           (lambda _
             (substitute* "meson.build"
               ((".*postinstall.*") "")))))))
    ;; (inputs
    ;;  (list jack-1 lv2 mesa))
    (native-inputs
     (list gettext-minimal
           gjs
           `(,glib "bin")               ; for 'glib-compile-resources'
           pkg-config))
    (home-page "https://github.com/tchx84/flatseal")
    (synopsis "Manage Flatpak permissons")
    (description "")
    (license license:gpl3+)))
