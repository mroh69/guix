; SPDX-FileCopyrightText: 2020-2023 mike@rohleder.de
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (mroh guix packages python)
  #:use-module (mroh guix packages galacteek)
  #:use-module (guix packages)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (guix build-system python)
  #:use-module (guix build-system pyproject)
  #:use-module (guix gexp)
  #:use-module (gnu packages)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages lsof)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages time)
  #:use-module (gnu packages crypto)
  #:use-module (gnu packages libffi)
  #:use-module (gnu packages check)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-crypto)
  #:use-module (gnu packages sqlite)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-check)
  #:use-module (gnu packages python-compression)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages serialization)
  #:use-module (gnu packages sphinx)
  #:use-module (gnu packages python-check)
  #:use-module (gnu packages messaging)
  #:use-module (gnu packages monitoring)
  #:use-module (gnu packages matrix)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages web)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu services)
  #:use-module (gnu services shepherd))

(define-public python-pysaml2-next
  (package
   (inherit python-pysaml2)
   (name "python-pysaml2-next")
   (version "7.2.1")
   (source
    (origin
     (method url-fetch)
     (uri (pypi-uri "pysaml2" version))
     (sha256
      (base32
       "07qbdni1f1dga8r37gj8ja13djpfzxvr45v9chayzbz9viv9a3zl"))))
   (propagated-inputs
    (list python-cryptography
          python-dateutil
          python-defusedxml
          python-importlib-resources
          python-pyopenssl
          python-pytz
          python-requests
          python-six
          python-xmlschema))))

(define-public python-twisted-next
  (package
    (inherit python-twisted)
    (name "python-twisted-next")
    (version "22.8.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "Twisted" version))
              (sha256
               (base32
                "1v9va71qygm5b6knyi6c3yrbvz73by44v1z1zd9s279dkzihvdp5"))))
    (build-system python-build-system)
    (arguments
     '(#:tests? #f))
    (native-inputs (list `(,python "tk")))
    (propagated-inputs
     (list
      python-pyasn1
      python-zope-interface
      python-pyhamcrest
      python-incremental-next
      python-hyperlink
      python-typing-extensions
      python-cryptography
      python-constantly
      python-bcrypt
      python-automat))))

(define-public python-incremental-next
  (package
    (inherit python-incremental)
    (name "python-incremental-next")
    (version "21.3.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "incremental" version))
       (sha256
        (base32
         "0mvgbmsnv1c8ziydw41jjkivc0zcqyzli7frcpvbkxj8zxddxx82"))))))

;; XXX: upstream (core-updates, because of python-pyopenssl)
(define-public python-setuptools-rust-next
  (package
    (inherit python-setuptools-rust)
    (name "python-setuptools-rust-next")
    (version "1.3.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "setuptools-rust" version))
       (sha256
        (base32
         "1pnim8yg1b2hpl7znhja6l351k3i3093i1c8mffxb0v4mgs5p34m"))))))

(define-public python-treq-next
  (package
    (inherit python-treq)
    (name "python-treq-next")
    (version "20.9.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "treq" version))
       (sha256
        (base32
         "18kdk11d84lyxj6dz183nblc6c6r4rj1hk0lpsyiykzgbakjrkc3"))))
    (propagated-inputs
     (list python-attrs
           python-idna
           python-incremental-next
           python-requests
           python-service-identity
           python-twisted-next))))

(define-public python-mautrix
  (package
   (name "python-mautrix")
   (version "0.20.4")
   (source
    (origin
     (method url-fetch)
     (uri (pypi-uri "mautrix" version))
     (sha256
      (base32
       "14l6l4mpyl0j9n7594cw4pa89y8dxkc8ir26x0zwix5icyjg2rxx"))))
   (build-system python-build-system)
   (arguments
    `(#:tests? #f                       ; needs network
               ))
   (propagated-inputs
    (list
     python-aiohttp
     python-attrs
     python-yarl))
   (home-page "https://github.com/tulir/mautrix-python")
   (synopsis "A Python 3 asyncio Matrix framework.")
   (description "A Python 3 asyncio Matrix framework.")
   (license license:mpl2.0)))

(define-public python-mautrix-18
  (package
   (name "python-mautrix-18")
   (version "0.18.9")
   (source
    (origin
     (method url-fetch)
     (uri (pypi-uri "mautrix" version))
     (sha256
      (base32
       "18nsfkq7z553p81inalbjgcdj0ivaq03zhpdnalbxx615kzb65i2"))))
   (build-system python-build-system)
   (arguments
    `(#:tests? #f                       ; needs network
               ))
   (propagated-inputs
    (list
     python-aiohttp
     python-attrs
     python-yarl))
   (home-page "https://github.com/tulir/mautrix-python")
   (synopsis "A Python 3 asyncio Matrix framework.")
   (description "A Python 3 asyncio Matrix framework.")
   (license license:mpl2.0)))

(define-public python-mautrix-15
  (package
   (inherit python-mautrix)
   (name "python-mautrix-15")
   (version "0.15.8")
   (source
    (origin
     (method url-fetch)
     (uri (pypi-uri "mautrix" version))
     (sha256
      (base32
       "06nzhx8byzbq3cc1lp278fkpbpkvkfgyy2nwf8z6mak2623n31y5"))))))

;; TODO upstream
(define-public mroh-python-ruamel.yaml
  (package
   (inherit python-ruamel.yaml)
   (name "mroh-python-ruamel.yaml")
   (version "0.17.21")
   (source
    (origin
     (method url-fetch)
     (uri (pypi-uri "ruamel.yaml" version))
     (sha256
      (base32
       "1bxp4dfkzzsndf9f8fspr4jc85nwf5243b616lm7a4pjlabycz4b"))))))

(define-public python-matrix-common
  (package
    (name "python-matrix-common")
    (version "1.3.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "matrix_common" version))
       (sha256
        (base32 "1ldby8prjxc2dgypmim5lycb3bj4viv7mhvynlbk894zrp623qb2"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-attrs))
    (native-inputs (list python-pytest python-setuptools python-wheel))
    (home-page "https://github.com/matrix-org/matrix-python-common")
    (synopsis "Common utilities for Synapse, Sydent and Sygnal")
    (description "Common utilities for Synapse, Sydent and Sygnal")
    (license license:asl2.0)))

(define-public python-mautrix-facebook
  (package
    (name "python-mautrix-facebook")
    (version "0.5.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "mautrix-facebook" version))
       (sha256
        (base32
         "1028z50l14fz49cv5frh0ql5bylp9596xfp825kcqr8dx21vh2zp"))))
    (build-system python-build-system)
    (arguments
     `(#:tests? #f))
    (propagated-inputs
     (list
      python-chardet                    ;is this missing in aiohttp?
      python-commonmark
      python-paho-mqtt
      python-pycryptodome
      python-magic
      python-mautrix
      mroh-python-ruamel.yaml
      python-zstandard
      python-asyncpg))
    (home-page "https://github.com/tulir/mautrix-facebook")
    (synopsis "A Matrix-Facebook Messenger puppeting bridge.")
    (description "A Matrix-Facebook Messenger puppeting bridge.")
    (license license:agpl3+)))

(define-public python-mautrix-signal
  (package
    (name "python-mautrix-signal")
    (version "0.4.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "mautrix-signal" version))
       (sha256
        (base32 "1r1hcim13wsxvn8082726gyq5nq2nd62vqhm4l6fipj57mvbv426"))))
    (build-system python-build-system)
    (arguments
     `(#:tests? #f))
    (propagated-inputs
     (list python-aiohttp
           python-asyncpg
           python-attrs
           python-chardet
           python-commonmark
           python-pycryptodome
           python-magic
           python-mautrix-18
           python-olm
           mroh-python-ruamel.yaml
           python-unpaddedbase64
           python-yarl))
    (home-page "https://github.com/mautrix/signal")
    (synopsis "A Matrix-Signal puppeting bridge.")
    (description "This package provides a Matrix-Signal puppeting bridge.")
    (license license:agpl3+)))

(define-public matrix-archive
  (package
   (name "matrix-archive")
   (version "0.0.0-3")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/russelldavies/matrix-archive")
           (commit "ee987cae29818faf7339537df1be8352953dde26")))
     (file-name (git-file-name name version))
     (sha256
      (base32
       "16cpqm2xaz9frmqvi9dz1jn0kyahhidh8rhm0qqz4gh8bwdvfvq7"))))
   (build-system python-build-system)
   (arguments
    `(#:tests? #f                       ;TODO
      #:phases
      (modify-phases %standard-phases
                     (delete 'build)
                     (replace 'install
                       (lambda* (#:key outputs #:allow-other-keys)
                         (let ((out (assoc-ref outputs "out")))
                           (install-file "matrix-archive.py" (string-append out "/bin"))))))))
   (propagated-inputs
    (list python-logbook
          python-aiofiles
          python-atomicwrites
          python-aiohttp
          python-pyaml
          python-matrix-nio))
   (home-page "https://github.com/russelldavies/matrix-archive")
   (synopsis "")
   (description "")
   (license license:asl2.0)))

(define-public matrix-commander
  (package
    (name "matrix-commander")
    (version "6.0.1")
    (home-page "https://github.com/8go/matrix-commander")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url home-page)
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "0m0xs0i1csis33l0kkcjrc711dfsrq3p3nfwbp889jr38hchqaim"))))
    (build-system pyproject-build-system)
    (arguments
     (list
      #:tests? #f                       ;TODO
      #:phases
      #~(modify-phases %standard-phases
          (delete 'sanity-check)        ;XXX:  selfreference?
          (add-after 'install 'install-bashcompletion
            (lambda _
              (let ((completiondir
                     (string-append #$output "/share/bash-completion/completions")))
                (mkdir-p completiondir)
                (copy-file "auto-completion/bash/matrix-commander.bash"
                           (string-append completiondir "/" #$name))))))))
    (native-inputs (list python-setuptools python-wheel))
    (propagated-inputs
     (list python-aiohttp
           python-aiofiles
           python-dbus
           python-pyxdg
           python-logbook
           python-markdown
           python-notify2
           python-magic
           python-pillow
           python-urllib3
           python-matrix-nio))
    (synopsis "simple but convenient CLI-based Matrix client app for sending and receiving")
    (description "simple but convenient CLI-based Matrix client app for sending and receiving")
    (license license:gpl3)))

(define-public synadm
  (package
   (name "synadm")
   (version "0.45")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/JOJ0/synadm")
           (commit (string-append "v" version))))
     (file-name (git-file-name name version))
     (sha256
      (base32
       "0jn41v2wwgcmzbyyk2042v5rkry85nq1n3al39lvjrcv8dp1b9xi"))))
   (build-system python-build-system)
   (propagated-inputs
    (list python-requests
          python-tabulate
          python-click-7
          python-click-option-group
          python-dnspython
          python-pyyaml))
   (home-page "https://github.com/JOJ0/synadm")
   (synopsis
    "a command line interface to the admin APIs of Matrix-Synapse collaboration servers ")
   (description
    "a command line interface to the admin APIs of Matrix-Synapse collaboration servers ")
   (license license:gpl3)))

(define-public python-click-option-group
  (package
   (name "python-click-option-group")
   (version "0.5.3")
   (source
    (origin
     (method url-fetch)
     (uri (pypi-uri "click-option-group" version))
     (sha256
      (base32
       "0pk1w2s1jh5fj5fb6skn9ci5p3hgjdz9yrvjbgmpyrbbqkrj9sd6"))))
   (build-system python-build-system)
   (propagated-inputs
    (list python-click-7))
   (native-inputs
    (list python-coverage
          python-coveralls
          python-pytest
          python-pytest-cov))
   (home-page
    "https://github.com/click-contrib/click-option-group")
   (synopsis "Option groups missing in Click")
   (description "Option groups missing in Click")
   (license license:expat)))

(define-public heisenbridge
  (package
   (name "heisenbridge")
   (version "1.14.2")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/hifi/heisenbridge")
           (commit (string-append "v" version))))
     (file-name (git-file-name name version))
     (sha256
      (base32
       "0j5yaphr0ns14bg7j6vhr4bh5cx4wyxjdlh62391z8k2zc8l2zxd"))))
   (build-system python-build-system)
   (arguments
    `(#:tests? #f))
   (propagated-inputs
    (list python-socks
          python-mautrix-15
          python-irc
          mroh-python-ruamel.yaml))
   (home-page "https://github.com/hifi/heisenbridge")
   (synopsis "a bouncer-style Matrix IRC bridge")
   (description "a bouncer-style Matrix IRC bridge")
   (license license:expat)))

(define-public python-irc
  (package
   (name "python-irc")
   (version "20.1.0")
   (source (origin
            (method url-fetch)
            (uri (pypi-uri "irc" version))
            (sha256
             (base32
              "1v0771aqwk8z20yjdskbb8ld40mggmxxw6g330xiyyfl5n9kgxxn"))))
   (build-system pyproject-build-system)
   (arguments
     (list
      #:tests? #f))
   (propagated-inputs (list python-importlib-metadata
                            python-jaraco-collections
                            python-jaraco-functools
                            python-jaraco-logging
                            python-jaraco-stream
                            python-jaraco-text
                            python-more-itertools
                            python-pytz
                            python-tempora))
   (native-inputs (list python-pygments
                        python-setuptools
                        python-setuptools-scm
                        python-pytest
                        python-pytest-black
                        python-pytest-checkdocs
                        python-pytest-cov
                        python-pytest-enabler
                        python-pytest-flake8
                        python-pytest-mypy
                        python-wheel))
   (home-page "https://github.com/jaraco/irc")
   (synopsis "IRC (Internet Relay Chat) protocol library for Python")
   (description "IRC (Internet Relay Chat) protocol library for Python")
   (license license:expat)))

(define-public python-jaraco-collections
  (package
   (name "python-jaraco-collections")
   (version "3.5.2")
   (source (origin
            (method url-fetch)
            (uri (pypi-uri "jaraco.collections" version))
            (sha256
             (base32
              "1hyj1m64vd1rgz91yadghk6g2km3cr756map9048br7r6pmr6aq7"))))
   (build-system pyproject-build-system)
   (arguments
     (list
      #:tests? #f))
   (propagated-inputs (list python-jaraco-classes python-jaraco-text))
   (native-inputs (list python-setuptools
                        python-setuptools-scm
                        python-pytest
                        python-pytest-black
                        python-pytest-checkdocs
                        python-pytest-cov
                        python-pytest-enabler
                        python-pytest-flake8
                        python-pytest-mypy
                        python-wheel))
   (home-page "https://github.com/jaraco/jaraco.collections")
   (synopsis "Collection objects similar to those in stdlib by jaraco")
   (description "Collection objects similar to those in stdlib by jaraco")
   (license license:expat)))

(define-public python-jaraco-logging
  (package
   (name "python-jaraco-logging")
   (version "3.1.2")
   (source (origin
            (method url-fetch)
            (uri (pypi-uri "jaraco.logging" version))
            (sha256
             (base32
              "0cbrwrcjqb7n97kwmfvay5lppmyfg62yjvmmqzb9lxv76y5hp9wk"))))
   (build-system pyproject-build-system)
   (arguments
     (list
      #:tests? #f))
   (propagated-inputs (list python-tempora))
   (native-inputs (list python-flake8
                        python-setuptools
                        python-setuptools-scm
                        python-pytest
                        python-pytest-black
                        python-pytest-checkdocs
                        python-pytest-cov
                        python-pytest-enabler
                        python-pytest-flake8
                        python-wheel))
   (home-page "https://github.com/jaraco/jaraco.logging")
   (synopsis "Support for Python logging facility")
   (description "Support for Python logging facility")
   (license license:expat)))

(define-public python-jaraco-stream
  (package
   (name "python-jaraco-stream")
   (version "3.0.3")
   (source (origin
            (method url-fetch)
            (uri (pypi-uri "jaraco.stream" version))
            (sha256
             (base32
              "0hf49mg5ya48saly4j30zsic8l1zz4lhv4yydnynbvlh212b1x1s"))))
   (build-system python-build-system)
   (native-inputs (list python-more-itertools
                        python-pytest
                        python-pytest-black
                        python-pytest-checkdocs
                        python-pytest-cov
                        python-pytest-enabler
                        python-pytest-flake8
                        python-pytest-mypy))
   (home-page "https://github.com/jaraco/jaraco.stream")
   (synopsis "routines for dealing with data streams")
   (description "routines for dealing with data streams")
   (license license:expat)))

(define-public python-jaraco-text
  (package
   (name "python-jaraco-text")
   (version "3.9.1")
   (source (origin
            (method url-fetch)
            (uri (pypi-uri "jaraco.text" version))
            (sha256
             (base32
              "1yxkyb5xa94a63wjzf4j7g16zycpx2a43q15hhqj102qi92d8z6m"))))
   (build-system pyproject-build-system)
   (arguments
     (list
      #:tests? #f))
   (propagated-inputs (list python-autocommand
                            python-importlib-resources
                            python-inflect
                            python-jaraco-context
                            python-jaraco-functools
                            python-more-itertools))
   (native-inputs (list python-flake8
                        python-pathlib2
                        python-setuptools
                        python-setuptools-scm
                        python-pytest
                        python-pytest-black
                        python-pytest-checkdocs
                        python-pytest-cov
                        python-pytest-enabler
                        python-pytest-flake8
                        python-pytest-mypy
                        python-wheel))
   (home-page "https://github.com/jaraco/jaraco.text")
   (synopsis "Module for text manipulation")
   (description "Module for text manipulation")
   (license license:expat)))

(define-public python-inflect
  (package
   (name "python-inflect")
   (version "6.0.0")
   (source (origin
            (method url-fetch)
            (uri (pypi-uri "inflect" version))
            (sha256
             (base32
              "0rb3zab5xz6lrgccy2d2rvqvcg098li62ykh4612spkjq9p53h8b"))))
   (build-system pyproject-build-system)
   (arguments
     (list
      #:tests? #f))
   (propagated-inputs (list python-pydantic))
   (native-inputs (list python-pygments
                        python-setuptools
                        python-setuptools-scm
                        python-pytest
                        python-pytest-black
                        python-pytest-checkdocs
                        python-pytest-cov
                        python-pytest-enabler
                        python-pytest-flake8
                        python-pytest-mypy
                        python-wheel))
   (home-page "https://github.com/jaraco/inflect")
   (synopsis
    "Correctly generate plurals, singular nouns, ordinals, indefinite articles; convert numbers to words")
   (description
    "Correctly generate plurals, singular nouns, ordinals, indefinite articles;
convert numbers to words")
   (license license:expat)))

(define-public python-opsdroid
  (package
   (name "python-opsdroid")
   (version "0.28.0")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/opsdroid/opsdroid")
           (commit (string-append "v" version))))
     (file-name (git-file-name name version))
     (sha256
      (base32
       "1qpd69skz3saav7bixi8vajhg4gk00h6mnd8j8kix3hrgz79pzjr"))))
   (build-system python-build-system)
   (arguments
    (list
     #:tests? #f                        ;TODO
     #:phases
     #~(modify-phases %standard-phases ;; TODO
                      (delete 'sanity-check))))
   (propagated-inputs
    (list python-aiohttp
          ;; ("python-aioredis" ,python-aioredis)
          python-aiosqlite
          python-appdirs
          python-arrow
          python-babel
          python-bitstring
          python-bleach
          python-certifi
          python-click
          ;; python-emoji python-ibm-watson
          python-matrix-nio
          ;; (python-mattermostdriver)
          ;; (python-motor)
          python-multidict
          python-nbconvert
          python-nbformat
          python-opsdroid-get-image-size
          python-parse
          python-puremagic
          python-pycron
          python-pyyaml
          python-regex
          python-rich
          ;; (python-slackclient)
          python-tailer
          python-udatetime
          python-voluptuous
          python-watchgod
          ;; (python-webexteamssdk)
          python-websockets))
   (home-page "https://opsdroid.github.io/")
   (synopsis
    "An open source ChatOps bot framework.")
   (description
    "An open source ChatOps bot framework.")
   (license license:asl2.0)))

(define-public python-tailer
  (package
    (name "python-tailer")
    (version "0.4.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "tailer" version))
       (sha256
        (base32
         "0cd5gf3iaddkzhg8lpdp85waqhhin038qg0b80px78mql4ihzmkq"))))
    (build-system python-build-system)
    (home-page "http://github.com/six8/pytailer")
    (synopsis
     "Python tail is a simple implementation of GNU tail and head.")
    (description
     "Python tail is a simple implementation of GNU tail and head.")
    (license license:expat)))

(define-public python-pycron
  (package
    (name "python-pycron")
    (version "3.0.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "pycron" version))
       (sha256
        (base32
         "1pmkmk9yb5gmx102zffs13kc8wil9b33mpv8ki0dalw27r7085mr"))))
    (build-system python-build-system)
    (arguments ;; FIXME: see pendulum
     `(#:tests? #f))
    (propagated-inputs
     `(("python-udatetime" ,python-udatetime)
       ("python-pytz" ,python-pytz)
       ;; ("python-pendulum" ,python-pendulum)  ;; XXX doesnt build
       ))
    (home-page "https://github.com/kipe/pycron")
    (synopsis
     "Simple cron-like parser, which determines if current datetime matches conditions.")
    (description
     "Simple cron-like parser, which determines if current datetime matches conditions.")
    (license license:expat)))

(define-public python-udatetime
  (package
    (name "python-udatetime")
    (version "0.0.17")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "udatetime" version))
       (sha256
        (base32
         "00c31gx0fmbs704z45pxpviqxhnqdgkn8nib5skki94r0rbwa2xi"))))
    (build-system python-build-system)
    (home-page "https://github.com/freach/udatetime")
    (synopsis
     "Fast RFC3339 compliant date-time library")
    (description
     "Fast RFC3339 compliant date-time library")
    (license license:asl2.0)))

(define-public python-puremagic
  (package
    (name "python-puremagic")
    (version "1.12")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "puremagic" version))
       (sha256
        (base32
         "0f3qakh6kin1lz34s06fjxg1vjbqjqpcmyih58v1f4wji70aw2wc"))))
    (build-system python-build-system)
    (arguments
    `(#:tests? #f))
    (home-page
     "https://github.com/cdgriffith/puremagic")
    (synopsis
     "Pure python implementation of magic file detection")
    (description
     "Pure python implementation of magic file detection")
    (license license:expat)))

;; XXX egg-info seems broken (again)
(define-public python-opsdroid-get-image-size
  (package
    (name "python-opsdroid-get-image-size")
    (version "0.2.2")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/opsdroid/image_size")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "0w19zw6vd7sl3b2fpzzlal09ffyndxyjgmhbd0s217ppadjc1xd9"))))
    ;; (source
    ;;  (origin
    ;;    (method url-fetch)
    ;;    (uri (pypi-uri "opsdroid-get-image-size" version))
    ;;    (sha256
    ;;     (base32
    ;;      "124j2xvfxv09q42qfb8nqlcn55y7f09iayrix3yfyrs2qyzav78a"))))
    (build-system python-build-system)
    (home-page
     "https://github.com/opsdroid/image_size")
    (synopsis "")
    (description "")
    (license license:expat)))

;; TODO guix bump (core-updates).
(define-public mroh-python-appdirs
  (package
    (inherit python-appdirs)
    (name "mroh-python-appdirs")
    (version "1.4.4")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "appdirs" version))
       (sha256
        (base32
         "0hfzmwknxqhg20aj83fx80vna74xfimg8sk18wb85fmin9kh2pbx"))))))

(define-public python-cryptography-fernet-wrapper
  (package
    (name "python-cryptography-fernet-wrapper")
    (version "1.0.4")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "python-cryptography-fernet-wrapper" version))
       (sha256
        (base32 "14ka27x72lb7aiqqw05lzxf5xhwww436pfma3qffng3nmnpiiwbl"))))
    (build-system python-build-system)
    (propagated-inputs (list python-cryptography))
    (home-page
     "https://github.com/KrazyKirby99999/python-cryptography-fernet-wrapper")
    (synopsis "A wrapper for cryptography.fernet")
    (description "This package provides a wrapper for cryptography.fernet")
    (license license:gpl3)))

(define-public python-simplematrixbotlib
  (package
    (name "python-simplematrixbotlib")
    (version "2.8.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "simplematrixbotlib" version))
       (sha256
        (base32 "0pnqsq2x11n28smv4hdqahiq77j2q9xbkgs2c9lfw3lqdfav7kqq"))))
    (build-system python-build-system)
    (propagated-inputs
     (list python-cryptography-fernet-wrapper
           python-markdown
           python-matrix-nio-19
           python-pillow
           python-toml))
    (home-page "https://codeberg.org/imbev/simplematrixbotlib")
    (synopsis
     "An easy to use bot library for the Matrix ecosystem written in Python.")
    (description
     "An easy to use bot library for the Matrix ecosystem written in Python.")
    (license license:expat)))

(define-public python-matrix-nio-19
  (package
    (inherit python-matrix-nio)
    (version "0.19.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "matrix-nio" version))
       (sha256
        (base32 "0v1mr5df7wjazksbicaa1l5mw2kkqjm3q3y9k1khyf2pnhnkwhgv"))))
    (arguments (list #:tests? #f))
    (propagated-inputs
     (list python-aiofiles
           python-aiohttp
           python-aiohttp-socks
           python-atomicwrites
           python-cachetools
           python-future
           python-h11
           python-h2
           python-jsonschema
           python-logbook
           python-olm
           python-peewee
           python-pycryptodome
           python-unpaddedbase64))))

(define-public python-backoff
  (package
    (name "python-backoff")
    (version "2.2.1")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "backoff" version))
              (sha256
               (base32
                "1fjwz9x81wpfn22j96ck49l3nb2hn19qfgv44441h8qrpgsjky03"))))
    (build-system python-build-system)
    (home-page "https://github.com/litl/backoff")
    (synopsis "Function decoration for backoff and retry")
    (description "Function decoration for backoff and retry")
    (license license:expat)))

(define-public python-ndjson
  (package
    (name "python-ndjson")
    (version "0.3.1")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "ndjson" version))
              (sha256
               (base32
                "1mp556r9ddw9wvj430zjbxk6sy3wq1ag39ydfb8m7jxidg5ld5xz"))))
    (build-system pyproject-build-system)
    (arguments
     `(#:tests? #f))
    (home-page "https://github.com/rhgrant10/ndjson")
    (synopsis "JsonDecoder for ndjson")
    (description "JsonDecoder for ndjson")
    (license license:gpl3)))

(define-public python2-feedparser
  (package
    (name "python2-feedparser")
    (version "5.2.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "feedparser" version ".tar.bz2"))
       (sha256
        (base32
         "00hb4qg2am06g81mygfi1jsbx8830024jm45g6qp9g8fr6am91yf"))))
    (build-system python-build-system)
    (arguments
     `(#:tests? #f
       #:python ,python-2))
    (home-page
     "https://github.com/kurtmckee/feedparser")
    (synopsis "Parse feeds in Python")
    (description
     "Universal feed parser which handles RSS 0.9x, RSS 1.0, RSS 2.0,
CDF, Atom 0.3, and Atom 1.0 feeds.")
    (license (list license:bsd-2        ; source code
                   license:freebsd-doc))))

(define-public pnntprss
  (package
   (name "pnntprss")
   (version "0.0.1")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/dpw/pnntprss/")
           (commit "58f2337e2e87ef333201d8806cd69776c22d46d1")))
     (file-name (git-file-name name version))
     (sha256
      (base32
       "1qmyy7apmdvgynjsjm90gzj1pm3yq5y6cfiarhm5ixlwmpqyxz7c"))))
   (build-system python-build-system)
   (arguments
    `(#:python ,python-2
      #:tests? #f
      #:phases
      (modify-phases %standard-phases
                     (delete 'configure)
                     (delete 'build)
                     (replace 'install
                              (lambda* (#:key outputs #:allow-other-keys)
                                (let ((out (assoc-ref outputs "out")))
                                  (for-each (lambda (file)
                                              (chmod file #o555)
                                              (install-file file (string-append out "/bin")))
                                            (find-files "." ".py"))))))))
   (native-inputs
    `(("python2-feedparser" ,python2-feedparser)))
   (home-page "https://github.com/dpw/pnntprss/")
   (synopsis "A Python RSS/Atom -> NNTP gateway")
   (description "A Python RSS/Atom -> NNTP gateway")
   (license license:expat)))
