; SPDX-FileCopyrightText: 2023 mike@rohleder.de
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (mroh guix packages synapse)
  #:use-module (mroh guix packages python)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix build-system cargo)
  #:use-module (guix build-system python)
  #:use-module (gnu packages check)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages matrix)
  #:use-module (gnu packages monitoring)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-crypto)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages rust)
  #:use-module (gnu packages xml))

;; (define-public mroh-synapse
;;   (package
;;    (name "mroh-synapse")
;;    (version "1.95.0")
;;    (source
;;     (origin
;;      (method url-fetch)
;;      (uri (pypi-uri "matrix_synapse" version))
;;      (sha256
;;       (base32
;;        "0lbxdmfw2q26lg1r55iv6735jfhqxg52pq4z7x4ncy1g9yjcc882"))))
;;    (build-system python-build-system)
;;    (arguments
;;     (list
;;      #:imported-modules (append %cargo-build-system-modules
;;                                 %python-build-system-modules)
;;      #:modules `(((guix build cargo-build-system) #:prefix cargo:)
;;                  ,@%python-build-system-modules
;;                  (srfi srfi-1)
;;                  (ice-9 match))
;;      #:phases
;;      #~(modify-phases
;;         (@ (guix build python-build-system) %standard-phases)
;;         (add-before 'build 'configure-cargo
;;          (lambda* (#:key inputs #:allow-other-keys)
;;            ;; Hide irrelevant inputs from cargo-build-system so it does
;;            ;; not try to unpack sanity-check.py, etc.
;;            (let ((cargo-inputs (filter (match-lambda
;;                                          ((name . path)
;;                                           (or (string-prefix? "rust-" name)
;;                                               (string=? "gcc" name))))
;;                                        inputs)))
;;              (with-directory-excursion "."
;;                                        ((assoc-ref cargo:%standard-phases 'unpack-rust-crates)
;;                                         #:inputs cargo-inputs
;;                                         #:vendor-dir "guix-vendor")
;;                                        ((assoc-ref cargo:%standard-phases 'configure)
;;                                         #:inputs cargo-inputs)
;;                                        ((assoc-ref cargo:%standard-phases 'patch-cargo-checksums)
;;                                         #:vendor-dir "guix-vendor")))))
;;         (replace 'check
;;                  (lambda* (#:key tests? #:allow-other-keys)
;;                    (when tests?
;;                      ;; for some reason, python doesnt load synapse_rust if cwd==project root
;;                      ;; found in aur: https://aur.archlinux.org/cgit/aur.git/tree/PKGBUILD?h=matrix-synapse-git
;;                      (mkdir-p "testdir")
;;                      (chdir "testdir")
;;                      (symlink "../tests" "tests")
;;                      (invoke "pytest" "-vv"
;;                              "-c" "/dev/null" ;take control over pytest options
;;                              ;; not sure why these test_disconnect fail.
;;                              "--ignore" "tests/test_server.py"
;;                              "--ignore" "tests/federation/transport/server/test__base.py"
;;                              "--ignore" "tests/replication/http/test__base.py"
;;                              ;; seems to handle warnings as error?
;;                              "--ignore" "tests/storage/test_room_search.py"
;;                              ;; needs network
;;                              "--ignore" "tests/http/"
;;                              "--ignore" "tests/replication/test_multi_media_repo.py")))))))
;;    (propagated-inputs
;;     (list
;;      python-matrix-common
;;      python-ijson
;;      python-jsonschema
;;      python-frozendict
;;      python-unpaddedbase64
;;      python-canonicaljson
;;      python-signedjson
;;      python-pynacl
;;      python-idna
;;      python-service-identity
;;      python-twisted-next
;;      python-treq-next
;;      python-pyopenssl
;;      python-pyyaml
;;      python-pyasn1
;;      python-pyasn1-modules
;;      python-daemonize
;;      python-bcrypt
;;      python-pillow
;;      python-sortedcontainers
;;      python-pymacaroons
;;      python-msgpack
;;      python-phonenumbers
;;      python-six
;;      python-prometheus-client
;;      python-attrs
;;      python-netaddr
;;      python-bleach
;;      python-typing-extensions
;;      python-pydantic
;;      python-cryptography
;;      ;; conditional requirements (synapse/python_dependencies.py)
;;      ;; hiredis is not a *strict* dependency, but it makes things much faster.
;;      ;; if redis ("python-hiredis" ,python-hiredis)
;;      python-matrix-synapse-ldap3-next
;;      python-psycopg2
;;      python-jinja2
;;      python-pysaml2-next
;;      python-lxml
;;      python-packaging
;;      ;; sentry-sdk, jaeger-client, and opentracing could be included, but
;;      ;; all are monitoring aids and not essential.
;;      python-pyjwt))
;;    (native-inputs
;;     (list python-mock
;;           python-parameterized
;;           python-setuptools-rust-next
;;           rust
;;           `(,rust "cargo")
;;           python-pytest))
;;    (inputs
;;     (list perl                          ;for sync_room_to_group.pl
;;           rust-pyo3-0.19
;;           rust-blake2-0.10
;;           rust-hex-0.4
;;           rust-cfg-if-1
;;           rust-indoc-1
;;           rust-libc-0.2
;;           rust-parking-lot-0.11
;;           rust-pyo3-ffi-0.19
;;           rust-pyo3-macros-0.19
;;           rust-pyo3-macros-backend-0.19
;;           rust-unindent-0.1
;;           rust-pyo3-build-config-0.19
;;           rust-pyo3-log-0.8
;;           rust-pythonize-next
;;           rust-crypto-mac-0.8
;;           rust-digest-0.10
;;           rust-opaque-debug-0.3
;;           rust-instant-0.1
;;           rust-lock-api-0.4
;;           rust-parking-lot-core-0.8
;;           rust-proc-macro2-1
;;           rust-quote-1
;;           rust-syn-1
;;           rust-once-cell-1
;;           rust-target-lexicon-0.12
;;           rust-generic-array-0.14
;;           rust-subtle-2
;;           rust-scopeguard-1
;;           rust-smallvec-1
;;           rust-redox-syscall-0.2
;;           rust-winapi-0.3
;;           rust-serde-1
;;           rust-serde-json-1
;;           rust-serde-derive-1
;;           rust-itoa-1
;;           rust-ryu-1
;;           rust-autocfg-1
;;           rust-typenum-1
;;           rust-version-check-0.9
;;           rust-bitflags-1
;;           rust-anyhow-1
;;           rust-lazy-static-1
;;           rust-log-0.4
;;           rust-regex-1
;;           rust-aho-corasick-1
;;           rust-regex-syntax-0.8
;;           rust-syn-2
;;           rust-regex-automata-0.4
;;           rust-memoffset-0.9
;;           rust-arc-swap-1
;;           rust-aho-corasick-0.7
;;           rust-memchr-2
;;           rust-block-buffer-0.10
;;           rust-crypto-common-0.1))
;;    (home-page "https://github.com/matrix-org/synapse")
;;    (synopsis "Matrix reference homeserver")
;;    (description "Synapse is a reference \"homeserver\" implementation of
;; Matrix from the core development team at matrix.org, written in
;; Python/Twisted.  It is intended to showcase the concept of Matrix and let
;; folks see the spec in the context of a codebase and let you run your own
;; homeserver and generally help bootstrap the ecosystem.")
;;    (license license:asl2.0)))

(define-public rust-pythonize-next
  (package
    (name "rust-pythonize-next")
    (version "0.19.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "pythonize" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0gnhrsc6mgpr0xp94wnf5dnw9dz5r58znjhv5mzfbb1hshbbfdcf"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-pyo3" ,rust-pyo3-0.19)
                       ("rust-serde" ,rust-serde-1))
       #:cargo-development-inputs (("rust-maplit" ,rust-maplit-1)
                                   ("rust-pyo3" ,rust-pyo3-0.19)
                                   ("rust-serde" ,rust-serde-1)
                                   ("rust-serde-json" ,rust-serde-json-1))))
    (native-inputs (list python-wrapper))
    (home-page "https://github.com/davidhewitt/pythonize")
    (synopsis
     "Serde Serializer & Deserializer from Rust <--> Python, backed by PyO3.")
    (description
     "Serde Serializer & Deserializer from Rust <--> Python, backed by PyO3.")
    (license license:expat)))


(define-public python-matrix-synapse-ldap3-next
  (package
    (inherit python-matrix-synapse-ldap3)
    (name "python-matrix-synapse-ldap3-next")
    (version "0.1.5")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "matrix-synapse-ldap3" version))
       (sha256
        (base32
         "1zjnah927dch42d08zqdywj1jbqb3jrm7shgm916cxgcr3vqvpwz"))))
    (propagated-inputs
     (list python-twisted-next python-ldap3 python-service-identity))))
